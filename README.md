# Kubitus installer <!-- omit in toc -->

Configuration management (Ansible + ArgoCD) of Kubernetes clusters and shared in-cluster infrastructure
applications.

![Kubitus logo](kubitus.png)

[Kubitus installer](https://gitlab.com/kubitus-project/kubitus-installer)
is an opinionated collection of Ansible playbooks to install
a full-featured Kubernetes cluster, leveraging GitOps semantics.

Table of Contents:

- [Architecture](#architecture)
- [Components](#components)
- [Installation](#installation)
- [Usage](#usage)
- [Contributing](#contributing)

## Architecture

![Kubitus architecture](nodes.drawio.png)

There are several kind of nodes:

- A Kubitus node, managing all the other nodes,
  and holding the PKI
- An optional Gitlab node, storing artifacts for offline access
- An optional Gitlabracadabra node, mirroring artifacts from internet
  to Gitlab. **Only the Gitlabracadabra node needs internet access**.
- An optional Kubespray node, installing Kubernetes and
  accessing the Kubernetes API. Alternatively, you can use
  terraform to create a cluster on GKE, or
  use an existing cluster
- Kubernetes nodes: control plane and workers

In a multi-cluster installation, you can either share or split the
out-of-cluster roles (Kubitus, Gitlab, Gitlabracadabra, Kubespray).

## Components

Notes :

- :eye: Common variables are in [kubitus_defaults](doc/kubitus_defaults.md).
- *(auto)* in the *Default enabled* column means that the role is enabled
   when there is a group with the same name in the inventory

| Name (Role) | Default enabled | Doc :book:, manual steps :writing_hand: | Installation method (with operator :bulb:) |
| ----------- | --------------- | --- | ----- |
| <div style="text-align: right">***External components*** :</div> | | | *(Ansible)* `external_components.yml` |
| **Terraform** (Provisioning) | (auto) | [:book:](doc/terraform.md) | *(Ansible)* |
| General **prerequisites** (filesystems, mount, files, ...) | &#9745; | [:book:](doc/prerequisites.md) | *(Ansible)* |
| **Upgrade** (host upgrade) | &#9744; | [:book:](doc/upgrade.md) | *(Ansible)* |
| **Bind** (DNS) | (auto) | [:book:](doc/bind.md) | *(Ansible)* |
| **Resolvconf** (DNS client config, i.e. `/etc/resolv.conf`) | (when bind is) | [:book:](doc/resolvconf.md) | *(Ansible)* |
| **PKI** (Private Key Infrastructure) | &#9745; | [:book:](doc/pki.md) | *(Ansible)* |
| **Certificate authority** (Creates several Certificate Authorities) | &#9745; | [:book:](doc/certificate_authority.md) | *(Ansible)* |
| **Certs** (Copy certificates to hosts) | &#9745; |  | *(Ansible)* |
| ISC-**DHCP** | (auto) | [:book:](doc/dhcp.md) | *(Ansible)* |
| **Kea** (DHCP) | (auto) | [:book:](doc/kea.md) | *(Ansible)* |
| **Gitlab** (Artefacts repository) | (auto) | [:book:](doc/gitlab.md) | *(Ansible)* |
| **GitLabracadabra** (Artifacts synchronization) | (auto) | [:book:](doc/gitlabracadabra.md) + [docker](doc/docker.md) | *(Ansible)* |
| **Govc** (VMware CLI) | (auto) | [:book:](doc/govc.md) | *(Ansible)* |
| **Kubectl** (Install `kubectl`) | (when relevant) | [:book:](doc/kubectl.md) | *(Ansible)* |
| **Helm** (Install `helm`) | &#9745; | [:book:](doc/helm.md) | *(Ansible)* |
| **K9s** (Install `k9s` a Kubernetes CLI) | &#9744; | [:book:](doc/k9s.md) | *(Ansible)* |
| **GitOps** (GitOps repository) | &#9745; | [:book:](doc/gitops.md) | *(Ansible)* |
| **[`kind`](https://kind.sigs.k8s.io/)** (Kubernetes IN Docker) | (auto) | [:book:](doc/kind.md) + [docker](doc/docker.md) | *(Ansible)* |
| **[Kubespray](https://github.com/kubernetes-sigs/kubespray)** (Kubernetes installation) | (auto) | [:book:](doc/kubespray.md) | *(Ansible)* |
| <div style="text-align: right">***Cluster*** :</div> | | | *(Ansible)* `apps.yml` |
| **Global** resources (`CustomResourceDefinition`, `ClusterRole` and `ClusterRoleBinding`) and resources in default namespaces (`NetworkPolicy`) | &#9745; | [:book:](doc/global.md) | *Helm@ArgoCD* |
| **Cilium** (Network) | &#9745; (unless GKE) | [:book:](doc/cilium.md) | *Helm* then *Helm@ArgoCD* |
| **ArgoCD** (GitOps) | &#9745; | [:book:](doc/argocd.md) | *Helm* then *Helm@ArgoCD* |
| **ArgoCD apps** (GitOps applications, application sets, extensions and projects) | &#9744; | [:book:](doc/argocd_apps.md) | *Helm@ArgoCD* |
| Open Policy Agent **[Gatekeeper](https://open-policy-agent.github.io/gatekeeper/website/docs/)** (Admission) | &#9745; | [:book:](doc/gatekeeper.md) | *Helm@ArgoCD* |
| **[Sealed Secrets](https://github.com/bitnami-labs/sealed-secrets#readme)** (Secrets) | &#9745; | [:book:](doc/sealed_secrets.md) | *Helm@ArgoCD* |
| **[cert-manager](https://cert-manager.io/)** (Certificates) | &#9745; | [:book:](doc/cert_manager.md) | *Helm@ArgoCD* |
| **[trust-manager](https://cert-manager.io/docs/trust/trust-manager/)** (Manage trust bundles) | &#9744; | [:book:](doc/trust_manager.md) | *Helm@ArgoCD* |
| Certificate injection | &#9744; | [:book:](doc/certificate_injection.md) | *Manifests@ArgoCD* |
| **MetalLB** (Load Balancer) | &#9744; | [:book:](doc/metallb.md) | *Helm@ArgoCD* |
| `ingress_nginx`, **[NGINX ingress controller](https://kubernetes.github.io/ingress-nginx/)** | &#9745; | [:book:](doc/ingress_nginx.md) | *Helm@ArgoCD* |
| **ExternalDNS** (DNS) | &#9744; | [:book:](doc/external_dns.md) | *Helm@ArgoCD* |
| **CSI Snapshotter** (Storage) | &#9744; | [:book:](doc/csi_snapshotter.md) | *Helm@ArgoCD* |
| **vSphere CSI** (Storage) | &#9744; | [:book:](doc/vsphere_csi.md) | *Helm@ArgoCD* |
| **Rook-Ceph** (Storage) | &#9745; | [:book:](doc/rook_ceph.md) | *Helm@ArgoCD* :bulb: |
| **[CloudNativePG](https://cloudnative-pg.io/)** (PostgreSQL) | &#9744; | [:book:](doc/cloudnative_pg.md) | *Helm@ArgoCD* |
| **KeyCloak**-Operator (IdP) | &#9744; | [:book:](doc/keycloak.md) | *OLM@ArgoCD* :bulb: |
| **Prometheus-stack** (Metrics) including Grafana (Metrics and logs WebUI) | &#9745; | [:book:](doc/prometheus_stack.md) | *Helm@ArgoCD* :bulb: |
| **[Prom Label Proxy](https://github.com/prometheus-community/prom-label-proxy/)** (Multi-tenant Prometheus Proxy) | &#9744; | [:book:](doc/prom_label_proxy.md) | *Helm@ArgoCD* |
| **[Prometheus Adapter](https://github.com/kubernetes-sigs/prometheus-adapter)** (custom.metrics.k8s.io API using Prometheus) | &#9744; | [:book:](doc/prometheus_adapter.md) | *Helm@ArgoCD* |
| **Loki** (Logs) | &#9745; | [:book:](doc/loki.md) | *Helm@ArgoCD* |
| **Promtail** (Logs) | &#9745; | [:book:](doc/promtail.md) | *Helm@ArgoCD* |
| **Fluent Bit** (Logds)  | &#9745; | [:book:](doc/fluent_bit.md) | *Helm@ArgoCD* |
| **OpenTelemetry Collector**s (collect metrics, logs and traces) | &#9744; | [:book:](doc/opentelemetry_collector.md) | *Helm@ArgoCD* |
| **OpenTelemetry Operator** (instrumentation) | &#9744; | [:book:](doc/opentelemetry_operator.md) | *Helm@ArgoCD* :bulb: |
| **Vault** (Secrets) | &#9744; | [:book:](doc/vault.md) :writing_hand: | *Helm@ArgoCD* :bulb: |
| **[MinIO](https://min.io/)** (Object Storage) | &#9744; | [:book:](doc/minio.md) | *Helm@ArgoCD* |
| **[MinIO Operator](https://min.io/)** (Object Storage) | &#9744; | [:book:](doc/minio_operator.md) | *Helm@ArgoCD* :bulb: |
| **[MinIO Tenant](https://min.io/)** (Object Storage) | &#9744; | [:book:](doc/minio_tenant.md) | *Helm@ArgoCD* |
| **Velero** (Backup) | &#9744; | [:book:](doc/velero.md) | *Helm@ArgoCD* :bulb: |
| **Trivy Operator** (Security toolkit) | &#9744; | [:book:](doc/trivy_operator.md) | *Helm@ArgoCD* |
| **GitLab Runner** (CI) | &#9744; | [:book:](doc/gitlab_runner.md) | *Helm@ArgoCD* |
| **[OpenCost](https://www.opencost.io)** (Cost monitoring) | &#9744; | [:book:](doc/opencost.md) | *Helm@ArgoCD* |
| **APIserver proxy** (Auth reverse-proxy to remote APIserver) | &#9744; | [:book:](doc/apiserver_proxy.md) | *Helm@ArgoCD* |

## Installation

Note: Installation has been tested on Debian 11 bullseye.

1. Install required packages and create directories

    - ansible >= 2.10
    - git
    - cfssl
    - python3-passlib

    ```shell
    sudo apt-get install -y ansible git golang-cfssl python3-passlib
    sudo mkdir -p /opt/kubitus/inventories
    sudo chown $USER /opt/kubitus /opt/kubitus/inventories
    ```

1. Clone this repository (alternatively, you can
   [download the code](https://gitlab.com/kubitus-project/kubitus-installer/-/archive/main/kubitus-installer-main.tar.gz))

    ```shell
    cd /opt/kubitus
    git clone https://gitlab.com/kubitus-project/kubitus-installer.git
    cd kubitus-installer
    ```

## Quick start

> This quick start is based on Kubespray. Alternatively, you can use [an existing cluster](doc/existing_cluster.md).

1. Create an inventory (replace `foobar`, and `*node`):

    ```bash
    inventory=foobar
    cp -a inventories/sample "../inventories/$inventory"

    cat <<EOF > "../inventories/$inventory/inventory.yml"
    gitlab:
      hosts:
        gitlabnode:

    gitlabracadabra:
      hosts:
        gitlabracadabranode:

    kubespray:
      hosts:
        localhost:
          ansible_connection: local

    kube_control_plane:
      hosts:
        controlplane1:
        controlplane2:
        controlplane3:

    etcd:
      children:
        kube_control_plane:

    kube_node:
      hosts:
        worker1:
        worker2:
        worker3:
          node_labels:
            # Accepts MetalLB and ingress-nginx pods
            kubitus-project/public: 'true'
          node_taints:
          # Only schedule MetalLB and ingress-nginx pods
          - { key: kubitus-project/public, value: 'true', effect: 'NoSchedule'}
    EOF

    cat <<EOF > ansible.cfg
    [ssh_connection]
    pipelining=True
    ssh_args = -o ControlMaster=auto -o ControlPersist=30m -o ConnectionAttempts=100 -o UserKnownHostsFile=/dev/null
    #control_path = ~/.ssh/ansible-%%r@%%h:%%p
    [defaults]
    inventory = ../inventories/$inventory/inventory.yml
    host_key_checking=False
    inject_facts_as_vars = False
    interpreter_python = auto
    stdout_callback = yaml
    callback_whitelist = profile_tasks
    EOF

    editor "../inventories/$inventory/group_vars/all/all.yml"
    ```

2. Install external components:

    ```bash
    ansible-playbook \
        --ask-pass \
        --ask-become-pass \
        external_components.yml
    ```

3. (if using kubespray) As the last role outputs, connect to the `kubespray` node, and run:

    ```bash
    # Run the following as kubitus user
    sudo -u kubitus -i

    # Launch kubespray
    cd /opt/kubitus/kubespray
    . venv/bin/activate

    ansible-playbook \
      -i "inventory/$inventory/" \
      --ask-pass \
      --become --ask-become-pass \
      cluster.yml
    ```

4. Install apps on the cluster:

    ```bash
    ansible-playbook \
        --ask-pass \
        --ask-become-pass \
        apps.yml
    ```

5. If you have enabled Vault, initialize it: See [here](doc/vault.md#initialize-and-unseal-vault)

6. After some time, you can [check status of installed applications](doc/selfcheck.md):

    ```bash
    ansible-playbook \
        --ask-pass \
        --ask-become-pass \
        selfcheck.yml
    ```

## Contributing

- File bugs and feature requests in
  [GitLab issues](https://gitlab.com/kubitus-project/kubitus-installer/-/issues).
  Security issues should be marked as **confidential**.
- Propose documentation or code improvements in
  [GitLab merge requests](https://gitlab.com/kubitus-project/kubitus-installer/-/merge_requests).

  There is a step-by-step tutorial to [create a role for a GitOps application](doc/gitops_application.md).

  This repository enforces commit message convention, to check this locally install the
  [commitlint](https://github.com/conventional-changelog/commitlint/#what-is-commitlint)
  hook:

  ```shell
  npm install  @commitlint/{config-conventional,cli}
  echo 'npx commitlint --edit' >> .git/hooks/commit-msg
  chmod +x .git/hooks/commit-msg
  ```

NB: There is also a documentation on how to [release a new version](doc/release.md) .
