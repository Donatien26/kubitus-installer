# ArgoCD

## Scope

The `argocd` role installs [ArgoCD](https://argo-cd.readthedocs.io/), a GitOps
tool, in the cluster.

## Accessing ArgoCD

The ArgoCD `"admin"` password is available from a secret:

```console
$ ARGOCD_INITIAL_ADMIN_PASSWORD="$(kubectl get secrets -n argocd argocd-initial-admin-secret -ojson | jq .data.password -r | base64 -d)"
$ echo $ARGOCD_INITIAL_ADMIN_PASSWORD
AbCdEf
```

This password can be used for full-access on the ArgoCD web interface,
or for the CLI:

```console
$ argocd login
Username: admin
Password:
'admin:login' logged in successfully
Context 'port-forward' updated
$ argocd app list
[...]
```

## ArgoCD application

Do not call this directly, use [GitOps application instead](gitops#gitops-application).

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/argocd.yml).

GitOps variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `argocd_enabled` | Enable ArgoCD | `true` |
| `argocd_description` | Description | `Argo CD` |
| `argocd_extra_namespaces` | Extra namespaces | `"{{ ['kube-system'] if kube_dns_stub_domains else [] }}"` |
| `argocd_helm_dependencies` | Helm dependencies | :page_with_curl: |
| `argocd_extra_namespace_resource_whitelist` | Extra namespace resource whitelist | :page_with_curl: |

Other public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `argocd_force_helm_install` | Force ArgoCD installation with Helm | `false` |
| `argocd_repo_username` | ArgoCD git repository user (and GitLab username) | `kubitus-argocd-{{ kubitus_inventory_name }}` |
| `argocd_gitlab_name` | GitLab name | `Kubitus ArgoCD {{ kubitus_inventory_name }}` |
| `argocd_gitlab_email` | GitLab email | ` "{{ argocd_repo_username }}@{{ top_level_domain }}"` |
| `argocd_repo_password` | ArgoCD git repository password (and GitLab password) | `"{{ lookup('password', kubitus_credentials_dir + '/argocd_repo_password.creds length=32') }}"` |
| `argocd_fqdn` | ArgoCD DNS name | `"argocd.{{ top_level_domain }}"` |
| `argocd_ingress_certificate_issuer` | ArgoCD ingress certificate issuer. See [`default_ingress_certificate_issuer`](kubitus_defaults.md#variables) for possible values | `"{{ default_ingress_certificate_issuer }}"` |
| `argocd_tls_certs_data` | TLS certificates, using fqdn as key, and bundle as value. For git repos and more. Example: `{othergitlab.example.org: "{{ local_ca_certificates.values() \| join('\n') }}"}` | `{}` |
| `argocd_oidc_enabled` | Enable OIDC | `"{{ keycloak_enabled }}"` |
| `argocd_oidc_issuer` | OIDC issuer URL | `"https://{{ keycloak_fqdn }}/auth/realms/default"` |
| `argocd_oidc_client_id` | OIDC client ID | `argocd` |
| `argocd_oidc_client_secret` | OIDC client secret  | `null` |
| `argocd_oidc_root_ca` | OIDC root CA. | :page_with_curl: (use PKI when using integrated keycloak) |
| `argocd_admin_enabled` | Enable admin user | `true` |
| `argocd_rbac_default_policy` | RBAC policy for users not matched in `argocd_rbac_permissions`. Example: `role:readonly` or `role:sync`  | `''` |
| `argocd_rbac_roles` | Roles | :page_with_curl: (creates a `role:sync`) |
| `argocd_rbac_permissions` | Permissions. Example: `g, argocd-admin, role:sync` | `''` |
| `argocd_create_aggregate_roles` | Create `ClusterRoles` that extend existing `ClusterRoles` to interact with argo-cd crds | `false` |
| `argocd_application_syncpolicy_automated` | Enable ArgoCD [Automated Sync Policy](https://argo-cd.readthedocs.io/en/stable/user-guide/auto_sync/), with `prune`, `allowEmpty` and `selfHeal` flags set to `true` | `true` |
| `argocd_controller_memory_request` | | `1Gi` |
| `argocd_controller_cpu_request` | | `10m` |
| `argocd_controller_memory_limit` | | `4Gi` |
| `argocd_controller_cpu_limit` | | `null` |
| `argocd_redis_memory_request` | | `100Mi` |
| `argocd_redis_cpu_request` | | `2m` |
| `argocd_redis_memory_limit` | | `200Mi` |
| `argocd_redis_cpu_limit` | | `null` |
| `argocd_server_memory_request` | | `150Mi` |
| `argocd_server_cpu_request` | | `2m` |
| `argocd_server_memory_limit` | | `300Mi` |
| `argocd_server_cpu_limit` | | `null` |
| `argocd_reposerver_memory_request` | | `400Mi` |
| `argocd_reposerver_cpu_request` | | `6m` |
| `argocd_reposerver_memory_limit` | | `800Mi` |
| `argocd_reposerver_cpu_limit` | | `null` |
| `argocd_applicationset_memory_request` | | `55Mi` |
| `argocd_applicationset_cpu_request` | | `null` |
| `argocd_applicationset_memory_limit` | | `55Mi` |
| `argocd_applicationset_cpu_limit` | | `null` |
| `argocd_notifications_memory_request` | | `45Mi` |
| `argocd_notifications_cpu_request` | | `null` |
| `argocd_notifications_memory_limit` | | `128Mi` |
| `argocd_notifications_cpu_limit` | | `null` |
| `argocd_chart_version` | | :page_with_curl: |
| `argocd_version` | | :page_with_curl: |
| `argocd_image` | | :page_with_curl: |
| `argocd_redis_image` | | :page_with_curl: |
| `argocd_redis_exporter_image` | | :page_with_curl: |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `argocd_projects` |  | :page_with_curl: |
| `argocd_downloads` |  | :page_with_curl: |
