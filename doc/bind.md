# Bind

## Scope

The `bind` role installs a DNS server.

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/bind.yml).

Public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `bind_external_enabled` | Enable bind | When the `bind` group is not empty: `"{{ groups['bind'] \| default([]) \| length > 0 }}"` |
| `bind_forwarders` | Forwarders. Example: `[8.8.8.8]` | `null` |
| `bind_ip_hostvar` | Host variable used as IP in DNS `A` records | `ip` |
| `bind_records` | Bind zone entries | :page_with_curl: |
