# Kubespray <!-- omit in toc -->

- [Scope](#scope)
- [Accessing the Kubernetes API](#accessing-the-kubernetes-api)
  - [From the Kubespray node](#from-the-kubespray-node)
  - [From the outside](#from-the-outside)
- [Upgrading Kubernetes](#upgrading-kubernetes)
- [Variables](#variables)

## Scope

The `kubespray` role installs a Kubernetes cluster using
[kubespray](https://github.com/kubernetes-sigs/kubespray/).

The [GitLab](gitlab.md) and [Gitlabracadabra](gitlabracadabra.md) roles
are required for offline mode.

The `kubespray` role has the following main steps:

- Sync the kubespray git repository to Gitlab
- Clone the synced repo on the kubespray node
- Sync the kubespray python requirements to Gitlab
- Create a virtualenv and install kubespray requirements
- Build the kubespray inventory
- Sync kubespray packages and images
- Print commands to be run from the kubespray node

## Accessing the Kubernetes API

### From the Kubespray node

The `kubitus` user can use the `kubectl` command on the Kubespray
node (with autocompletion):

```shell
$ kubectl get all
NAME                 TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
service/kubernetes   ClusterIP   10.233.0.1   <none>        443/TCP   4h19m
```

The following commands are also available:

- `helm`
- `argocd`
- `kubeseal`

### From the outside

The `cluster-admin` `kubeconfig` is available on the Kubespray node at
`/var/opt/kubitus/config/$inventory/kubespray-artifacts/admin.conf`.

:warning: This file gives all privileges to the Kubernetes cluster.
Ensure you keep it secure!

## Upgrading Kubernetes

The steps to upgrade a cluster are similar to
[initial installation](../README.md), but with the
`upgrade-cluster.yml` playbook:

1. Prepare the kubespray installation:

    ```bash
    ansible-playbook \
        --ask-pass \
        --ask-become-pass \
        --tags kubespray \
        external_components.yml
    ```

2. Connect to the `kubespray` node, and run:

    ```bash
    # Run the following as kubitus user
    sudo -u kubitus -i

    # Launch kubespray
    cd /opt/kubitus/kubespray
    . venv/bin/activate

    inventory=foobar
    ansible-playbook \
      -i "inventory/$inventory/" \
      --ask-pass \
      --become --ask-become-pass \
      upgrade-cluster.yml
    ```

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/kubespray.yml).

Public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `kubespray_external_enabled` | Enable kubespray | When the `kubespray` group is not empty: `"{{ groups['kubespray'] \| default([]) \| length > 0 }}"` |
| `kubespray_version` | Kubespray version. Use `keep` to keep the Git repo in the current state | :page_with_curl: |
| `kubespray_allow_major_version_skip` | Allow to skip a Kubespray major version | `false` |
| `kubespray_hostvars_map` | Inject hostvars in kubespray inventory. Key is destination variable, value is source variable. Example: `{ip: ansible_host}` | `{}` |
| `kubespray_kube_vip_address` | Enable `kube-vip` as load-balancer for control-planes with the given address | `null` |
| `kubespray_extravars` | Kubespray [extra variables](https://github.com/kubernetes-sigs/kubespray/blob/master/docs/vars.md) | `{}` |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `kubespray_additional_requirements` | Additional Python packages to install | :page_with_curl: |
| `kubespray_pypi_url` | PyPI URL | `"https://{{ gitlab_fqdn }}/api/v4/projects/external-packages%2Fkubespray-pypi/packages/pypi/simple"` |
| `kubespray_inventory_dir` | Kubespray inventory directory (contains `kubespray-artifacts`, `kubespray-credentials`) | `"{{ kubitus_data_dir }}"` |
| `kubespray_repo_projects` |  | :page_with_curl: |
| `kubespray_pypi_projects` |  | :page_with_curl: |
| `kubespray_projects` |  | :page_with_curl: |
