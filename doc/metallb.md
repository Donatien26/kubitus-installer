# MetalLB

## Scope

The `metallb` role installs the [MetalLB](https://metallb.universe.tf/),
a load-balancer implementation.

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/metallb.yml).

GitOps variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `metallb_enabled` | Enable metallb | `false` |
| `metallb_description` | Description | `MetalLB` |
| `metallb_namespace` | Namespace | `metallb-system` |
| `metallb_namespace_pod_security` | Pod Security of the instance namespace | `privileged` |
| `metallb_helm_dependencies` | Helm dependencies | :page_with_curl: |
| `metallb_extra_namespace_resource_whitelist` | Extra namespace resource whitelist | :page_with_curl: |
| `metallb_ignore_differences` | Ignore differences | :page_with_curl: |

Other public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `metallb_ip_range` | Array of ip ranges. Example: `["10.5.0.50-10.5.0.99"]` | `[]` |
| `metallb_alert_addresspool_exhausted_enabled` | Alert when AddressPool is exhausted | `true` |
| `metallb_alert_addresspool_usage_enabled` | Alert for AddressPool usage | `true` |
| `metallb_alert_addresspool_usage_thresholds` | | :page_with_curl: |
| `metallb_controller_memory_request` |  | `50Mi` |
| `metallb_controller_cpu_request` |  | `5m` |
| `metallb_controller_memory_limit` |  | `100Mi` |
| `metallb_controller_cpu_limit` |  | `null` |
| `metallb_speaker_memory_request` |  | `80Mi` |
| `metallb_speaker_cpu_request` |  | `6m` |
| `metallb_speaker_memory_limit` |  | `120Mi` |
| `metallb_speaker_cpu_limit` |  | `null` |
| `metallb_chart_version` | | :page_with_curl: |
| `metallb_controller_image` | | :page_with_curl: |
| `metallb_speaker_image` | | :page_with_curl: |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `metallb_projects` |  | :page_with_curl: |
