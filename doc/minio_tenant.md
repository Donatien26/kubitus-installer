# MinIO Tenant

## Scope

The `minio_tenant` creates a [MinIO](min.io/) tenant.

The []`minio-operator`](minio_operator) role is required

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/minio_operator.yml).

GitOps variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `minio_tenant_enabled` | Enable MinIO Tenant | `false` |
| `minio_tenant_description` | Description | `MinIO tenant` |
| `minio_tenant_helm_dependencies` | Helm dependencies | :page_with_curl: |
| `minio_tenant_extra_namespace_resource_whitelist` | Extra namespace resource whitelist | :page_with_curl: |
| `minio_tenant_sealed_secrets` | Sealed secrets | `[minio-env-configuration,tls-api,tls-console]` |
| `minio_tenant_sealed_secret_minio_env_configuration_type` |  `default-ssl-certificate` secret type | `kubernetes.io/tls` |
| `minio_tenant_sealed_secret_minio_env_configuration_items` | `default-ssl-certificate` secret items | `[config.env]` |
| `minio_tenant_sealed_secret_minio_env_configuration_data_config_env_value` | `config.env` content | :page_with_curl: |
| `minio_tenant_sealed_secret_tls_api_type` | `tls-api` secret type | `kubernetes.io/tls` |
| `minio_tenant_sealed_secret_tls_api_items` | `tls-api` secret items | `[ca.crt, tls.crt, tls.key]` |
| `minio_tenant_sealed_secret_tls_api_data_ca_crt_value` | `ca.crt` content | :page_with_curl: |
| `minio_tenant_sealed_secret_tls_api_data_tls_key_value` | `tls.key` content | :page_with_curl: |
| `minio_tenant_sealed_secret_tls_api_data_tls_crt_value` | `tls.crt` content | :page_with_curl: |
| `minio_tenant_sealed_secret_tls_console_type` | `tls-console` secret type | `kubernetes.io/tls` |
| `minio_tenant_sealed_secret_tls_console_items` | `tls-console` secret items | `[ca.crt, tls.crt, tls.key]` |
| `minio_tenant_sealed_secret_tls_console_data_ca_crt_value` | `ca.crt` content | :page_with_curl: |
| `minio_tenant_sealed_secret_tls_console_data_tls_key_value` | `tls.key` content | :page_with_curl: |
| `minio_tenant_sealed_secret_tls_console_data_tls_crt_value` | `tls.crt` content | :page_with_curl: |
| `minio_tenant_memory_request` | [Memory request](https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/) | `128Mi` |
| `minio_tenant_memory_limit` | [Memory limit](https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/) | `2Gi` |

Tenant variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `minio_tenant_root_user` | MinIO Tenant Root User | :page_with_curl: |
| `minio_tenant_root_password` | MinIO Tenant Root Password | :page_with_curl: |
| `minio_tenant_pools` | Pools | `[{servers: 4, name: default, volumesPerServer: 1, size: 10Gi}]` |
| `minio_tenant_logging_quiet` | Disables startup information | `false` |
| `minio_tenant_logging_anonymous` | Hides sensitive information from logging | `false` |
| `minio_tenant_logging_json` | Outputs server logs and startup information in `JSON` format | `true` |

API variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `minio_tenant_api_ingress_enabled` | Enable API Ingress | `false` |
| `minio_tenant_api_ingress_certificate_issuer` | API Ingress certificate issuer. See [`default_ingress_certificate_issuer`](kubitus_defaults.md#variables) for possible values | `"{{ default_ingress_certificate_issuer }}"` |
| `minio_tenant_api_fqdn` | API DNS name | `"minio{{ gitops_instance_variables.suffix }}.{{ top_level_domain }}"` |
| `minio_tenant_api_service_type` | API service type | `ClusterIP` |
| `minio_tenant_api_load_balancer_ip` | API Loadbalancer IP address | `~` |
| `minio_tenant_api_service_from_cidrs` | API Allowed IP subnets (when set and service type is `LoadBalancer`) | `[]` |

Console variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `minio_tenant_console_ingress_enabled` | Console Ingress | `false` |
| `minio_tenant_console_ingress_certificate_issuer` | Console Ingress certificate issuer. See [`default_ingress_certificate_issuer`](kubitus_defaults.md#variables) for possible values | `"{{ default_ingress_certificate_issuer }}"` |
| `minio_tenant_console_fqdn` | Console DNS name | `"minio-operator{{ gitops_instance_variables.suffix }}.{{ top_level_domain }}"` |
| `minio_tenant_console_service_type` | Console service type | `ClusterIP` |
| `minio_tenant_console_load_balancer_ip` | Console Loadbalancer IP address | `~` |
| `minio_tenant_console_service_from_cidrs` | Console Allowed IP subnets (when set and service type is `LoadBalancer`) | `"{{ minio_tenant_api_service_from_cidrs }}"` |

OIDC variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `minio_tenant_oidc_enabled` | Enable OIDC | `false` |
| `minio_tenant_oidc_config_url` | OIDC config URL | `"https://{{ keycloak_fqdn }}/auth/realms/d_tenantefault/.well-known/openid-configuration"` |
| `minio_tenant_oidc_client_id` | OIDC client ID | `minio` |
| `minio_tenant_oidc_client_secret` | OIDC client secret | :page_with_curl: |
| `minio_tenant_oidc_claim_name` | OIDC claim name | `policy` |
| `minio_tenant_oidc_scopes` | OIDC scopes | `openid,profile,email` |
| `minio_tenant_oidc_redirect_uri` | OIDC redirect URI | `"https://{{ minio_console_fqdn }}/oauth_ca_tenantllback"` |
| `minio_tenant_oidc_claim_prefix` | OIDC claim prefix | `''` |
| `minio_tenant_oidc_comment` | OIDC comment | `''` |


Artefacts variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `minio_tenant_chart_version` |  | :page_with_curl: |
| `minio_tenant_image` |  | :page_with_curl: |
| `minio_tenant_client_image` |  | :page_with_curl: |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `minio_tenant_projects` |  | :page_with_curl: |
| `minio_tenant_downloads` |  | :page_with_curl: |
