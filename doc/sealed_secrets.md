# Sealed Secrets

## Scope

The `sealed_secrets` role installs [Sealed Secrets](https://github.com/bitnami-labs/sealed-secrets#readme), to decrypt secrets from GitOps repository.

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/sealed_secrets.yml).

GitOps variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `sealed_secrets_enabled` | Sealed Secrets | `true` |
| `sealed_secrets_description` | Description | `Sealed Secrets` |
| `sealed_secrets_cascading_deletion` | Cascading deletion. One of `foreground`, `background` or `null` | `null` |
| `sealed_secrets_helm_dependencies` | Helm dependencies | :page_with_curl: |

Other public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `sealed_secrets_ingress_enabled` | | `false` |
| `sealed_secrets_fqdn` | | `"sealed-secrets.{{ top_level_domain }}"` |
| `sealed_secrets_ingress_certificate_issuer` | | `"{{ default_ingress_certificate_issuer }}"` |
| `sealed_secrets_force_seal` | Force reseal of sealed secrets even when hash is not changed  |`false` |
| `sealed_secrets_memory_request` | | `40Mi` |
| `sealed_secrets_cpu_request` | | `1m` |
| `sealed_secrets_memory_limit` | | `80Mi` |
| `sealed_secrets_cpu_limit` | | `2m` |
| `sealed_secrets_chart_version` | | :page_with_curl: |
| `sealed_secrets_version` | | :page_with_curl: |
| `sealed_secrets_image` | | :page_with_curl: |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `sealed_secrets_projects` |  | :page_with_curl: |
| `sealed_secrets_downloads` |  | :page_with_curl: |
