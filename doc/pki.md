# PKI

## Scope

The `pki` role creates a local PKI, with:

- An auto-signed root certificate
- An intermediate certificate
- A certificate for each item of `pki_certificates`

If `pki_external_enabled` is `false`, you'll have to provide the certificates for the
following roles:

- [GitLab](gitlab.md)
  - `pki/certificates/{{ gitlab_fqdn }}/cert-bundle.pem`
  - `pki/certificates/{{ gitlab_fqdn }}/cert-key.pem`
  - `pki/certificates/{{ gitlab_registry_fqdn }}/cert-bundle.pem`
  - `pki/certificates/{{ gitlab_registry_fqdn }}/cert-key.pem`
- [cert-manager](cert_manager.md)
  - `pki/intermediate_ca-key.pem`
  - `pki/intermediate_ca_signed.pem`
- [ingress-nginx](ingress_nginx.md)
  - `pki/certificates/_.{{ top_level_domain }}/cert-bundle.pem`
  - `pki/certificates/_.{{ top_level_domain }}/cert-key.pem`
  - `pki/certificates/_.{{ top_level_domain }}/cert.pem`

The PKI has the following files:

```console
$ tree pki
pki
├── ca-config.json
├── ca.csr
├── ca-csr.json
├── ca-key.pem
├── ca.pem
├── certificates
│   ├── _.k8s.example.org
│   │   ├── cert-bundle.pem
│   │   ├── cert.csr
│   │   ├── cert-key.pem
│   │   └── cert.pem
│   ├── gitlab.k8s.example.org
│   │   ├── cert-bundle.pem
│   │   ├── cert.csr
│   │   ├── cert-key.pem
│   │   └── cert.pem
│   ├── gitlab-registry.k8s.example.org
│   │   ├── cert-bundle.pem
│   │   ├── cert.csr
│   │   ├── cert-key.pem
│   │   └── cert.pem
│   └── minio.k8s.example.org
│       ├── cert-bundle.pem
│       ├── cert.csr
│       ├── cert-key.pem
│       └── cert.pem
├── intermediate_ca.csr
├── intermediate_ca-csr.json
├── intermediate_ca-key.pem
├── intermediate_ca.pem
├── intermediate_ca_signed.csr
└── intermediate_ca_signed.pem

3 directories, 19 files
```

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/pki.yml).

Public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `pki_external_enabled` | Enable PKI | `true` |
| `pki_dir` | Base directory of PKI | `"{{ playbook_dir }}/pki"` |
| `pki_certificates` | | :page_with_curl: |
