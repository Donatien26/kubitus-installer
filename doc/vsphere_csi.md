# vSphere CSI

## Scope

The `vsphere_csi` role installs the
[Container Storage Interface (CSI) driver for vSphere](https://github.com/kubernetes-sigs/vsphere-csi-driver) (also named *VMware vSphere Container Storage Plug-in*), providing `PersistentVolume`s.

## Splitted mode

In workload cluster inventories:

```yaml
vsphere_csi_enabled: true
vsphere_csi_global_mode: workload
# vsphere_csi_storageclass_*: ...
```

In management cluster:

```yaml
vsphere_csi_workloads:
- cluster_id: cluster1
  # kubeconfig: /var/opt/kubitus/config/cluster1/kubespray-artifacts/admin.conf
  # kubectl_delegate_to:
  # kubectl_become:
  # kubectl_user:
  vcenter_server: ...
  vcenter_thumbprint: ...
  vcenter_datacenters: ...
  vcenter_user: ...
  vcenter_password: ...
```

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/rook_ceph.yml).

GitOps variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `vsphere_csi_enabled` | Enable vSphere CSI | `false` |
| `vsphere_csi_description` | Description | `vSphere CSI` |
| `vsphere_csi_namespace_pod_security` | Pod Security of the instance namespace | `privileged` |
| `vsphere_csi_helm_dependencies` | Helm dependencies | :page_with_curl: |
| `vsphere_csi_extra_cluster_resource_whitelist` | Extra cluster resource whitelist | :page_with_curl: |
| `vsphere_csi_keep_file_paths` | Keep file paths | :page_with_curl: |
| `vsphere_csi_instance_default_keep_file_paths` | Keep file paths for default instance | `[]]` |

Other public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `vsphere_csi_global_mode` | Use `workload` on workload clusters. Requires [vsphere-tmm/helm-charts#50](https://github.com/vsphere-tmm/helm-charts/pull/50) | `null` |
| `vsphere_csi_storageclass_enabled` | | `true` |
| `vsphere_csi_storageclass_storagepolicyname` | | `""` |
| `vsphere_csi_storageclass_expansion` | | `true` |
| `vsphere_csi_storageclass_default` | | `false` |
| `vsphere_csi_cluster_id` | The unique cluster identifier. Each Kubernetes cluster must contain a unique cluster-id set in the configuration file. The cluster ID cannot not exceed 64 characters | `"{{ top_level_domain[0:64] }}"` |
| `vsphere_csi_vcenter_server` | | `null` |
| `vsphere_csi_vcenter_thumbprint` | | `null` |
| `vsphere_csi_vcenter_datacenters` | | `null` |
| `vsphere_csi_vcenter_user` | | `null` |
| `vsphere_csi_vcenter_password` | | `"{{ lookup('password', kubitus_credentials_dir + '/vsphere_csi_vcenter_password.creds length=32') }}"` |
| `vsphere_csi_workloads` | See [splitted mode](#splitted-mode) | `[]` |
| `vsphere_csi_only_workloads` | Set to an array of cluster_ids (see `vsphere_csi_workloads`) or `self` | not defined, defaults to all workloads |
| `vsphere_csi_controller_memory_request` | | `30Mi` |
| `vsphere_csi_controller_cpu_request` | | `null` |
| `vsphere_csi_controller_memory_limit` | | `90Mi` |
| `vsphere_csi_controller_cpu_limit` | | `null` |
| `vsphere_csi_controller_resizer_memory_request` | | `20Mi` |
| `vsphere_csi_controller_resizer_cpu_request` | | `null` |
| `vsphere_csi_controller_resizer_memory_limit` | | `40Mi` |
| `vsphere_csi_controller_resizer_cpu_limit` | | `null` |
| `vsphere_csi_controller_attacher_memory_request` | | `30Mi` |
| `vsphere_csi_controller_attacher_cpu_request` | | `null` |
| `vsphere_csi_controller_attacher_memory_limit` | | `60Mi` |
| `vsphere_csi_controller_attacher_cpu_limit` | | `null` |
| `vsphere_csi_controller_livenessprobe_memory_request` | | `15Mi` |
| `vsphere_csi_controller_livenessprobe_cpu_request` | | `null` |
| `vsphere_csi_controller_livenessprobe_memory_limit` | | `30Mi` |
| `vsphere_csi_controller_livenessprobe_cpu_limit` | | `null` |
| `vsphere_csi_controller_syncer_memory_request` | | `40Mi` |
| `vsphere_csi_controller_syncer_cpu_request` | | `null` |
| `vsphere_csi_controller_syncer_memory_limit` | | `160Mi` |
| `vsphere_csi_controller_syncer_cpu_limit` | | `null` |
| `vsphere_csi_controller_provisioner_memory_request` | | `30Mi` |
| `vsphere_csi_controller_provisioner_cpu_request` | | `null` |
| `vsphere_csi_controller_provisioner_memory_limit` | | `60Mi` |
| `vsphere_csi_controller_provisioner_cpu_limit` | | `null` |
| `vsphere_csi_controller_snapshotter_memory_request` | | `30Mi` |
| `vsphere_csi_controller_snapshotter_cpu_request` | | `1m` |
| `vsphere_csi_controller_snapshotter_memory_limit` | | `60Mi` |
| `vsphere_csi_controller_snapshotter_cpu_limit` | | `null` |
| `vsphere_csi_node_memory_request` | | `40Mi` |
| `vsphere_csi_node_cpu_request` | | `null` |
| `vsphere_csi_node_memory_limit` | | `80Mi` |
| `vsphere_csi_node_cpu_limit` | | `null` |
| `vsphere_csi_node_registrar_memory_request` | | `25Mi` |
| `vsphere_csi_node_registrar_cpu_request` | | `1m` |
| `vsphere_csi_node_registrar_memory_limit` | | `50Mi` |
| `vsphere_csi_node_registrar_cpu_limit` | | `null` |
| `vsphere_csi_livenessprobe_memory_request` | | `10Mi` |
| `vsphere_csi_livenessprobe_cpu_request` | | `null` |
| `vsphere_csi_livenessprobe_memory_limit` | | `20Mi` |
| `vsphere_csi_livenessprobe_cpu_limit` | | `null` |
| `vsphere_csi_chart_version` |  | :page_with_curl: |
| `vsphere_csi_driver_image` |  | :page_with_curl: |
| `vsphere_csi_syncer_image` |  | :page_with_curl: |
| `vsphere_csi_resizer_image` |  | :page_with_curl: |
| `vsphere_csi_attacher_image` |  | :page_with_curl: |
| `vsphere_csi_livenessprobe_image` |  | :page_with_curl: |
| `vsphere_csi_provisioner_image` |  | :page_with_curl: |
| `vsphere_csi_snapshotter_image` |  | :page_with_curl: |
| `vsphere_csi_snapshot_validation_webhook_image` |  | :page_with_curl: |
| `vsphere_csi_node_driver_registrar_image` |  | :page_with_curl: |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `vsphere_csi_projects` |  | :page_with_curl: |
