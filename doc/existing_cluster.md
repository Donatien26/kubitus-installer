# Using an existing cluster with direct internet access

1. Create an inventory (replace `foobar`):

    ```bash
    inventory=foobar
    cp -a inventories/sample "../inventories/$inventory"

    cat <<EOF > "../inventories/$inventory/inventory.yml"
    gitops:
      hosts:
        localhost:
          ansible_connection: local
    EOF

    cat <<EOF > ansible.cfg
    [ssh_connection]
    pipelining=True
    ssh_args = -o ControlMaster=auto -o ControlPersist=30m -o ConnectionAttempts=100 -o UserKnownHostsFile=/dev/null
    #control_path = ~/.ssh/ansible-%%r@%%h:%%p
    [defaults]
    inventory = ../inventories/$inventory/inventory.yml
    host_key_checking=False
    inject_facts_as_vars = False
    interpreter_python = auto
    stdout_callback = yaml
    callback_whitelist = profile_tasks
    EOF

    editor "../inventories/$inventory/group_vars/all/all.yml"
    ```

    Modify the following:

    ```yaml
    offline_enabled: false

    kubitus_data_dir: "{{ ansible_inventory_sources | last | dirname }}/data"
    gitops_repo_url: <path to your repo>.git  # Not an empty repo!
    gitops_repo_username: gitops  # Maintainer write_repository
    gitops_repo_password: a-token-with-write-access-to-repo
    argocd_repo_username: argocd  # Developper read_repository
    argocd_repo_password: a-token-with-read-access-to-repo

    kubeconfig: "{{ lookup('env', 'HOME') }}/.kube/config"  # i.e. ~/.kube/config
    kubectl: kubectl
    kubectl_delegate_to: localhost
    kubectl_become: false
    kubectl_user: "{{ lookup('env', 'USER') }}"
    kubectl_group: "{{ kubectl_user }}"

    cilium_enabled: false  # unless your cluster has no CNI installed
    rook_ceph_enabled: false  # unless your cluster has no CSI installed

2. Install external components:

    ```bash
    ansible-playbook \
        --ask-pass \
        --ask-become-pass \
        external_components.yml
    ```

3. Install apps on the cluster:

    ```bash
    ansible-playbook \
        --ask-pass \
        --ask-become-pass \
        apps.yml
    ```

