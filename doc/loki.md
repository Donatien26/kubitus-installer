# Loki

## Scope

The `loki` role installs [Loki](https://grafana.com/oss/loki/), as log
aggregation system.

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/loki.yml).

GitOps variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `loki_enabled` | Enable Loki | `true` |
| `loki_helm_dependencies` | Helm dependencies | :page_with_curl: |
| `loki_helm_skip_crds` | Helm skip CRDs | `true` |
| `loki_sealed_secrets` | Sealed Secrets | `[kubitus-basic-auth]` |
| `loki_sealed_secret_kubitus_basic_auth_items` | `kubitus-basic-auth` secret items | `[.htpasswd]` |
| `loki_sealed_secret_kubitus_basic_auth_data__htpasswd_value` | `.htpasswd` content | `"{{ lookup('file', kubitus_credentials_dir + '/loki_instance_default_htpasswd.creds') }}"` |

Other public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `loki_retention_enabled` | Enable [retention](https://grafana.com/docs/loki/latest/operations/storage/retention/) | `true` |
| `loki_retention_period` | | `744h` (31d) |
| `loki_split_queries_by_interval` | | `15m` |
| `loki_max_query_parallelism` | | `null` |
| `loki_max_outstanding_requests_per_tenant` | | `null` |
| `loki_max_outstanding_per_tenant` | | `null` |
| `loki_write_persistence_size` | | `10Gi` |
| `loki_read_persistence_size` | | `10Gi` |
| `loki_single_binary_persistence_size` | | `40Gi` |
| `loki_gateway_memory_request` | | `null` |
| `loki_gateway_cpu_request` | | `null` |
| `loki_gateway_memory_limit` | | `null` |
| `loki_gateway_cpu_limit` | | `null` |
| `loki_write_memory_request` | | `null` |
| `loki_write_cpu_request` | | `null` |
| `loki_write_memory_limit` | | `null` |
| `loki_write_cpu_limit` | | `null` |
| `loki_read_memory_request` | | `null` |
| `loki_read_cpu_request` | | `null` |
| `loki_read_memory_limit` | | `null` |
| `loki_read_cpu_limit` | | `null` |
| `loki_single_binary_memory_request` | | `1Gi` |
| `loki_single_binary_cpu_request` | | `10m` |
| `loki_single_binary_memory_limit` | | `12Gi` |
| `loki_single_binary_cpu_limit` | | `null` |
| `loki_chart_version` |  | :page_with_curl: |
| `loki_kubectl_image` |  | :page_with_curl: |
| `loki_image` |  | :page_with_curl: |
| `loki_gateway_image` |  | :page_with_curl: |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `loki_projects` |  | :page_with_curl: |
