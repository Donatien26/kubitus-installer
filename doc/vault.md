# Vault

## Scope

The `vault` role install an in-cluster Vault HA cluster.

## Initialize and unseal Vault

Follow the relevant section from the tutorial [here](https://learn.hashicorp.com/tutorials/vault/kubernetes-raft-deployment-guide#initialize-and-unseal-vault).

## Upgrading Vault

Once the upgrade of the `StatefullSet` has been done using Kubitus installer,
follow the relevant section from the tutorial [here](https://learn.hashicorp.com/tutorials/vault/kubernetes-raft-deployment-guide#upgrading-vault-servers).

## Authenticating kubectl using Vault

[kubecert.sh](../roles/vault/files/kubecert.sh) can be used to connect to kubectl with a PKI in vault.

The `~/.kube/config` file will look like:

```yaml
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: FIXME
    server: https://apiserver-cluster-workload.mgmt.example.org/
  name: apiserver-cluster-workload.mgmt.example.org
contexts:
- context:
    cluster: apiserver-cluster-workload.mgmt.example.org
    user: cert-mgmt
    namespace: default
  name: apiserver-cluster-workload.mgmt.example.org
current-context: apiserver-cluster-workload.mgmt.example.org
kind: Config
preferences: {}
users:
- name: cert-mgmt
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1
      command: bash
      args:
      - kubecert.sh
      env:
      - name: VAULT_ADDR
        value: https://vault.mgmt.example.org:8200
      # - name: VAULT_SKIP_VERIFY
      #   value: 'false'
      # - name: KUBECERT_COMMON_NAME_COMMAND
      #   value: echo "$USERNAME"
      interactiveMode: IfAvailable

```

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/vault.yml).

GitOps variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `vault_enabled` | Enable Vault | `false` |
| `vault_helm_dependencies` | Helm dependencies | :page_with_curl: |
| `vault_ignore_differences` | Ignore differences | :page_with_curl: |
| `vault_sealed_secrets` | Sealed secrets | :page_with_curl: |
| `vault_sealed_secret_tls_server_type` |  `tls-server` secret type | `kubernetes.io/tls` |
| `vault_sealed_secret_tls_server_items` | `tls-server` secret items | `[ca.crt, tls.crt, tls.key]` |
| `vault_sealed_secret_tls_server_data_ca_crt_value` | `ca.crt` content | `"{{ lookup('file', pki_dir + '/intermediate_ca_signed.pem') }}"` |
| `vault_sealed_secret_tls_server_data_tls_key_value` | `tls.key` content | `"{{ lookup('file', pki_dir + '/certificates/' + gitops_instance_variables.fqdn + '/cert-key.pem') }}"` |
| `vault_sealed_secret_tls_server_data_tls_crt_value` | `tls.crt` content | `"{{ lookup('file', pki_dir + '/certificates/' + gitops_instance_variables.fqdn + '/cert.pem') }}"` |
| `vault_sealed_secret_kubitus_items` | `kubitus` secret items | `[VAULT_TOKEN]` |

Other public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `vault_ingress_enabled` | Vault ingress enabled |  `false` |
| `vault_fqdn` | Vault DNS name | `"vault.{{ top_level_domain }}"` |
| `vault_ingress_certificate_issuer` | ArgoCD ingress certificate issuer. See [`default_ingress_certificate_issuer`](kubitus_defaults.md#variables) for possible values | `"{{ default_ingress_certificate_issuer }}"` |
| `vault_ha_replicas` | Number of replicas | `5` |
| `vault_cert_manager` | Use cert-manager to create Vault certificates | `"{{ cert_manager_enabled }}"` |
| `vault_telemetry_enabled` | Enable Vault telemetry and Prometheus integration |  `true` |
| `vault_injector_enabled` | Vault injector enabled |  `true` |
| `vault_ui_enabled` | Vault UI enabled |  `false` |
| `vault_ui_service_type` | Vault UI service type | `LoadBalancer` |
| `vault_ui_load_balancer_ip` | Load balancer IP for Vault UI service | `null` |
| `vault_csi_enabled` | Vault CSI enabled |  `false` |
| `vault_server_memory_request` | | `8Gi` |
| `vault_server_memory_limit` | | `16Gi` |
| `vault_server_cpu_request` | | `2000m` |
| `vault_server_cpu_limit` | | `2000m` |
| `vault_chart_version` | Vault chart version | :page_with_curl: |
| `vault_image` | Vault version | :page_with_curl: |
| `vault_k8s_image` | Vault k8s version | :page_with_curl: |
| `vault_csi_provider_image` | Vault CSI provider version (not used) | :page_with_curl: |
| `vault_transit_address` | The full address to the Vault cluster | `null` |
| `vault_transit_key_name` | The transit key to use for encryption and decryption | `autounseal` |
| `vault_transit_mount_path` | The mount path to the transit secret engine | `transit/` |
| `vault_transit_tls_ca_crt` | Content of the `ca.crt` file used for transit. Set to `null` to disable TLS verification | `"{{ lookup('file', pki_dir + '/intermediate_ca_signed.pem') }}"` |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `vault_projects` |  | :page_with_curl: |
| `vault_downloads` |  | :page_with_curl: |
