# GitOps

## Scope

The `gitops` role create a GitOps repo on GitLab, and clone it on the hosts.

This role is applied on the `gitops` and `kubespray` groups.

## GitOps application

>  There is a step-by-step tutorial to [create a role for a GitOps application](gitops_application.md).

A GitOps application can be declared with:

```yaml
- name: My Application | GitOps application
  include_role:
    name: gitops
    tasks_from: application
  vars:
    gitops_name: my-application
```

For each `<_gitops_slug>_instances` (default to `['default']`),
`gitops_instance_variables` is set with the following keys:

| Name | Description | Application default | Instance default |
| ---- | ----------- | ------------------- |----------------- |
| `.suffix` | Instance suffix *(Not overridable)* | *N/A* | `"{{ '' if gitops_instance == 'default' else ('-' + gitops_instance) }}"` |
| `.name` | Instance name *(Not overridable)* | *N/A* | `"{{ gitops_name + .suffix }}"` |
| `.slug` | Instance slug *(Not overridable)* | *N/A* | `"{{ (gitops_name + '-instance-' + gitops_instance) \| replace('-', '_') }}"` |
| `.role` | Instance role *(Not overridable)* | *N/A* | `"{{ gitops_name \| replace('-', '_') }}"` |
| `.description` | Instance description | `"{{ gitops_name \| capitalize }}"` | `"{{ _gitops_description + ' ' + gitops_instance }}"` |
| `.cascading_deletion` | Cascading deletion. One of `foreground`, `background` or `null` | `foreground` |
| `.namespace` | Instance namespace | `"{{ gitops_name }}"` | `"{{ _gitops_namespace + gitops_instance_suffix }}"` |
| `.create_namespace` | Create instance namespace | `true` | :arrow_left: |
| `.namespace_pod_security` | Pod Security of the instance namespace | `baseline` | :arrow_left: |
| `.extra_namespaces` | Instance extra namespaces | `[]` | `{{ _gitops_extra_namespaces \| map('regex_replace', '$', gitops_instance_suffix) }}` |
| `.helm_dependencies` | Helm dependencies. Example `[{name: my-application, version: "{{ my_application_chart_version}}", repository: "..." }]` | `[]` | :arrow_left: |
| `.ignore_differences` | Ignore differences. See [ArgoCD documentation](https://argoproj.github.io/argo-cd/user-guide/diffing/#application-level-configuration) | `[]` | :arrow_left: |
| `.keep_file_paths` | Keep file paths | `[]` | :arrow_left: |
| `.helm_skip_crds` | Skip CRDs | `false` | :arrow_left: |
| `.patches_json6902` |Patches JSON6902. Example below | `[]` | :arrow_left: |
| `.patches_strategic_merge` | Patches strategic merge. Example below | `[]` | :arrow_left: |
| `.server_side_apply` | Server-Side apply | `false` | :arrow_left: |
| `.extra_cluster_resource_whitelist` | Extra cluster resource whitelist. Example below | `[]` | :arrow_left: |
| `.extra_namespace_resource_whitelist` | Extra namespace resource whitelist. Example below | `[]` | :arrow_left: |
| `.ingress_class` | Ingress class | `"{{ kubitus_ingress_class }}"` | :arrow_left: |
| `.sealed_secrets` | Sealed secrets. Example below | `[]` | :arrow_left: |

Example variables:

```yaml
my_application_patches_json6902:
- target:
    group: apps
    version: v1
    kind: Deployment
    name: mypodinfo
  patch:
  - op: add
    path: /metadata/labels/yyyy
    value: xxxx
my_application_patches_strategic_merge:
- kind: Deployment
  apiVersion: apps/v1
  metadata:
    name: mypodinfo
    labels:
      xxxx: yyyy
my_application_extra_cluster_resource_whitelist:
- group: my-application.example.org
  kind: MyCRD

my_application_sealed_secrets:
- secret1
# my_application_sealed_secret_secret1_name: secret1
# my_application_sealed_secret_secret1_namespace: "{{ gitops_instance_variables.namespace }}"
# my_application_sealed_secret_secret1_labels: {}
# my_application_sealed_secret_secret1_type: "{{ omit }}"
# my_application_sealed_secret_secret1_unencrypted_data: {}
# my_application_sealed_secret_secret1_data: {}
# my_application_sealed_secret_secret1_items: []
my_application_sealed_secret_secret1_items:
- data1
# my_application_sealed_secret_secret1_data_data1_key: data1
# my_application_sealed_secret_secret1_data_data1_password_length: 64
# my_application_sealed_secret_secret1_data_data1_password_chars: ascii_letters,digits
# my_application_sealed_secret_secret1_data_data1_password_encrypt: ''
# my_application_sealed_secret_secret1_data_data1_value: "{{ lookup('password', kubitus_credentials_dir + '/' + my_application_sealed_secret_secret1_data_data1 + '.creds length=32 chars=ascii_letters,digits') }}"
```

Each GitOps application instance, has the following steps:

- Create directories in the gitops repository
- Call [ArgoCD application](argocd#argocd-application)

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/gitops.yml).

Public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `gitops_enabled` | Enable GitOps | `true` |
| `gitops_create_project` | Create repo on GitLab | `{{ gitlabracadabra_external_enabled }}` |
| `gitops_project` | GitLab project name | `"{{ kubitus_inventory_name }}/kubitus-gitops"` |
| `gitops_repo_url` | Git repository URL | `"https://{{ gitlab_fqdn }}/{{ gitops_project }}.git"` |
| `gitops_repo_username` | GitOps git repository user (and GitLab username) | `kubitus-gitops-{{ kubitus_inventory_name }}` |
| `gitops_repo_password` | GitOps git repository password (and GitLab password) | `"{{ lookup('password', kubitus_credentials_dir + '/gitops_repo_password.creds length=32') }}"` |
| `gitops_repo` | GitOps git repository local path | `"{{ kubitus_data_dir }}/infra"` |
| `gitops_author_name` | `GIT_AUTHOR_NAME` used for GitOps operations (and GitLab name) | `Kubitus GitOps {{ kubitus_inventory_name }}` |
| `gitops_author_email` | `GIT_AUTHOR_EMAIL` used for GitOps operations (and GitLab email) | `"{{ gitops_repo_username }}@{{ top_level_domain }}"` |
| `gitops_committer_name` | `GIT_COMMITTER_NAME` used for GitOps operations | `"{{ gitops_author_name }}"` |
| `gitops_committer_email` | `GIT_COMMITTER_EMAIL` used for GitOps operations | `"{{ gitops_author_email }}"` |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `gitops_projects` |  | :page_with_curl: |
