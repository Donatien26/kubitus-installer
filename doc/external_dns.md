# ExternalDNS

## Scope

The `external_dns` role installs [ExternalDNS](https://github.com/kubernetes-sigs/external-dns),
to synchronize exposed Kubernetes Services and Ingresses with DNS providers.

## Example config

```yaml
external_dns_enabled: true
external_dns_policy: sync
external_dns_domain_filters:
- "{{ top_level_domain }}"
external_dns_extra_args:
- --rfc2136-host=10.20.30.40
- --rfc2136-port=53
- "--rfc2136-zone={{ top_level_domain }}"
- --rfc2136-tsig-secret=FIXME
- --rfc2136-tsig-secret-alg=hmac-sha256
- --rfc2136-tsig-keyname=externaldns-key
- --rfc2136-tsig-axfr
```

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/external_dns.yml).

GitOps variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `external_dns_enabled` | Enable ExternalDNS | `false` |
| `external_dns_description` | Description | `ExternalDNS` |
| `external_dns_helm_dependencies` | Helm dependencies | :page_with_curl: |

Other public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `external_dns_sources` | List of sources. Item can be `ingress`, `service` | `[ingress]` |
| `external_dns_policy` | How DNS records are synchronized between source and providers, available values are `sync`, `upsert-only` | `upsert-only` |
| `external_dns_txt_prefix` | Prefix for the domain names of TXT records created by the `"txt"` registry | `external-dns.` |
| `external_dns_txt_suffix` | Suffix for the domain names of TXT records created by the `"txt"` registry | `""` |
| `external_dns_domain_filters` | List of allowed domains | `[]` |
| `external_dns_provider` | Provider | `rfc2136` |
| `external_dns_extra_args` | Extra args | `[]` |
| `external_dns_chart_version` |  | :page_with_curl: |
| `external_dns_image` |  | :page_with_curl: |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `external_dns_projects` |  | :page_with_curl: |
