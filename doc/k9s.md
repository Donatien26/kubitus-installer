# K9s

## Scope

The `k9s` role installs `k9s`, a Kubernetes CLI.

This role is applied on the `gitops` and `kubespray` groups.

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/k9s.yml).

Public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `k9s_enabled` | Enable K9s installation | `false` |
| `k9s_version` |  | :page_with_curl: |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `k9s_projects` |  | :page_with_curl: |
| `k9s_downloads` |  | :page_with_curl: |
