# Kubectl

## Scope

The `kubectl` role installs `kubectl`.

This role is applied on the `gitops` group.

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/kubectl.yml).

Public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `kubectl_enabled` | Enable Kubectl installation | `"{{ 'gitops' in group_names }}"` |
| `kubectl_version` |  | :page_with_curl: |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `kubectl_projects` |  | :page_with_curl: |
| `kubectl_downloads` |  | :page_with_curl: |
