# Keycloak

## Scope

The `keycloak` role installs the [Quarkus Keycloak Operator](https://github.com/keycloak/keycloak/),
the [legacy Operator](https://github.com/keycloak/keycloak-operator) (for missing CRDs)
and creates the needed Keycloak resources.

:fire: This role requires other roles:

- [CloudNativePG](cloudnative_pg.md) to setup a PostgreSQL database

## Identity providers

Example:

```yaml
keycloak_identity_providers:
- alias: external-oidc-provider
  providerId: oidc
  displayName: External OIDC provider
  enabled: true
  # updateProfileFirstLoginMode: on
  # trustEmail: false
  # storeToken: false
  # addReadTokenRoleOnCreate: false
  # authenticateByDefault: false
  # linkOnly: false
  # firstBrokerLoginFlowAlias: first broker login
  config:
    clientId: k8s-keycloak
    tokenUrl: https://auth.example.org/auth/realms/realm-name/protocol/openid-connect/token
    authorizationUrl: https://auth.example.org/auth/realms/realm-name/protocol/openid-connect/auth
    clientAuthMethod: client_secret_post
    logoutUrl: https://auth.example.org/auth/realms/realm-name/protocol/openid-connect/logout
    syncMode: IMPORT
    clientSecret: T0ken
    # useJwksUrl: "true"
```

## Accessing Keycloak

The Keycloak `admin` password is available from a secret:

```console
$ KEYCLOAK_ADMIN_PASSWORD="$(kubectl get secret -n keycloak keycloak-initial-admin -ojson | jq .data.password -r | base64 -d)"
$ echo $KEYCLOAK_ADMIN_PASSWORD
AbCdEf
```

This password can be used for full-access on the Keycloak web interface,
at `https://{{ keycloak_fqdn }}/auth/admin/master/console/#/`.

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/keycloak.yml).

GitOps variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `keycloak_enabled` | Enable Keycloak | `false` |
| `keycloak_description` | Description | `KeyCloak Operator` |
| `keycloak_namespace` | Namespace | `keycloak-operator` |
| `keycloak_extra_namespaces` | Extra namespaces | `[grafana, keycloak]` |
| `keycloak_helm_dependencies` | Helm dependencies | :page_with_curl: |
| `keycloak_extra_cluster_resource_whitelist` | Extra cluster resource whitelist | :page_with_curl: |
| `keycloak_extra_namespace_resource_whitelist` | Extra namespace resource whitelist | :page_with_curl: |
| `keycloak_sealed_secrets` | Sealed Secrets | `[keycloak-tls, keycloak-initial-admin, credential-keycloak-legacy]` |
| `keycloak_sealed_secret_keycloak_tls_namespace` |  `keycloak-tls` secret namespace | `keycloak` |
| `keycloak_sealed_secret_keycloak_tls_type` |  `keycloak-tls` secret type | `kubernetes.io/tls` |
| `keycloak_sealed_secret_keycloak_tls_items` | `keycloak-tls` secret items | `[ca.crt, tls.crt, tls.key]` |
| `keycloak_sealed_secret_keycloak_tls_data_ca_crt_value` | `ca.crt` content | `"{{ lookup('file', pki_dir + '/intermediate_ca_signed.pem') }}"` |
| `keycloak_sealed_secret_keycloak_tls_data_tls_key_value` | `tls.key` content | `"{{ lookup('file', pki_dir + '/certificates/' + gitops_instance_variables.fqdn + '/cert-key.pem') }}"` |
| `keycloak_sealed_secret_keycloak_tls_data_tls_crt_value` | `tls.crt` content | `"{{ lookup('file', pki_dir + '/certificates/' + gitops_instance_variables.fqdn + '/cert.pem') }}"` |
| `keycloak_sealed_secret_keycloak_initial_admin_namespace` | Namespace of the `keycloak-initial-admin` secret | `keycloak` |
| `keycloak_sealed_secret_keycloak_initial_admin_items` | Items of the `keycloak-initial-admin` secret | `[username, password]` |
| `keycloak_sealed_secret_keycloak_initial_admin_type` | Type of the `keycloak-initial-admin` secret | `kubernetes.io/basic-auth` |
| `keycloak_sealed_secret_keycloak_initial_admin_data_username_value` | | `admin` |
| `keycloak_sealed_secret_keycloak_initial_admin_data_password_value` | | `"{{ gitops_instance_variables.admin_password }}"` |
| `keycloak_sealed_secret_credential_keycloak_legacy_namespace` | Namespace of the `credential-keycloak-legacy` secret | `keycloak` |
| `keycloak_sealed_secret_credential_keycloak_legacy_items` | Items of the `credential-keycloak-legacy` secret | `[ADMIN_USERNAME, ADMIN_PASSWORD]` |
| `keycloak_sealed_secret_credential_keycloak_legacy_data_admin_username_value` | | `admin` |
| `keycloak_sealed_secret_credential_keycloak_legacy_data_admin_password_value` | | `"{{ gitops_instance_variables.admin_password }}"` |

Other public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `keycloak_admin_password` | Keycloak admin password | `"{{ lookup('password', kubitus_credentials_dir + '/keycloak_admin_password.creds length=32 chars=ascii_letters,digits') }}"` |
| `keycloak_fqdn` | Keycloak fqdn | `"auth.{{ top_level_domain }}"` |
| `keycloak_ingress_certificate_issuer` | Keycloak ingress certificate issuer. See [`default_ingress_certificate_issuer`](kubitus_defaults.md#variables) for possible values | `"{{ default_ingress_certificate_issuer }}"` |
| `keycloak_identity_providers` | See [above](#identity-providers)  | `[]` |
| `keycloak_operator_chart_version` | | :page_with_curl: |
| `keycloak_operator_image` | | :page_with_curl: |
| `keycloak_image` | | :page_with_curl: |
| `keycloak_operator_legacy_chart_version` | | :page_with_curl: |
| `keycloak_operator_legacy_image` | | :page_with_curl: |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `keycloak_projects` |  | :page_with_curl: |
