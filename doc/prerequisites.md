# Prerequisites

## Scope

The `prerequisites` role :

- updates `apt` cache on all nodes
- creates filesystems
- configure `/etc/fstab` and mounts

This role is applied on all hosts.

Example:

```yaml
prerequisites_files:
- path: /path/to/new/dir
  state: directory
  owner: root
  group: root
  mode: '0755'
prerequisites_copies:
- dest: /etc/sudoers.d/admins.conf
  owner: root
  group: root
  mode: '0640'
  content: |
    %admins ALL=(ALL:ALL) NOPASSWD: ALL
prerequisites_lineinfiles:
- path: /etc/default/grub
  line: GRUB_TIMEOUT=2
  regex: '^GRUB_TIMEOUT='
prerequisites_replaces:
- path: /etc/network/interfaces
  regexp: '^allow-hotplug eth0$'
  replace: "auto eth0"
prerequisites_apt_absent_packages:
- auditd
prerequisites_apt_present_packages:
- etckeeper
prerequisites_filesystems:
- dev: /dev/sdb
  fstype: ext4
  resizefs: true
  opts: -L containerd
prerequisites_mounts:
- src: LABEL=containerd
  path: /var/lib/containerd
  fstype: ext4
  opts: defaults
  state: mounted
prerequisites_rc_local_commands: |
  # https://github.com/cilium/cilium/21801
  ethtool -K eth0 tx-udp_tnl-segmentation off
  ethtool -K eth0 tx-udp_tnl-csum-segmentation off
```

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/prerequisites.yml).

Public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `prerequisites_files` | List of files, see example above | `[]` |
| `prerequisites_copies` | List of copies, see example above | `[]` |
| `prerequisites_lineinfiles` | List of lineinfiles, see example above | `[]` |
| `prerequisites_replaces` | List of replaces, see example above | `[]` |
| `prerequisites_apt_absent_packages` | Packages to uninstall | `[]` |
| `prerequisites_apt_present_packages` | Packages to install | `[]` |
| `prerequisites_filesystems` | List of filesystems, see example above | `[]` |
| `prerequisites_mounts` | List of mounts, see example above | `[]` |
| `prerequisites_rc_local_commands` | Commands in `rc.local`, see example above | `''` |
