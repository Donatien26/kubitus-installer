# Gatekeeper

## Scope

The `gatekeeper` role installs  Open Policy Agent
**[Gatekeeper](https://open-policy-agent.github.io/gatekeeper/website/docs/)**,
as an admission and mutation controller.

## Recover a cluster

Gatekeeper is configured to Fail Closed. As a consequence, a cluster can't
recover from a complete shutdown.

To recover from such state, set `KUBECONFIG` to the target cluster, and run:

```console
$ gatekeeper-cluster-recover
```

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/gatekeeper.yml).

GitOps variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `gatekeeper_enabled` | Enable Gatekeeper | `true` |
| `gatekeeper_namespace_pod_security` | Pod Security of the instance namespace | `restricted` |
| `gatekeeper_cascading_deletion` | Cascading deletion. One of `foreground`, `background` or `null` | `null` |
| `gatekeeper_extra_namespaces` | Extra namespaces | `[argocd]` |
| `gatekeeper_helm_dependencies` | Helm dependencies | :page_with_curl: |
| `gatekeeper_extra_cluster_resource_whitelist` | Extra cluster resource whitelist | :page_with_curl: |
| `gatekeeper_extra_namespace_resource_whitelist` | Extra namespace resource whitelist | :page_with_curl: |

Other public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `gatekeeper_mutating_webhook_timeout_seconds` | MutatingWebhookConfiguration timeout. See [open-policy-agent/gatekeeper#2132](https://github.com/open-policy-agent/gatekeeper/pull/2132) for discussion | `1` |
| `gatekeeper_validatingwebhook_exempt_resources` | Resources that are always exempted from the validating webhook | :page_with_curl: |
| `gatekeeper_validatingwebhook_extra_resources` | Resources that are always added in the validating webhook (even if exempted) | :page_with_curl: |
| `gatekeeper_mutatingwebhook_exempt_resources` | Resources that are always exempted from the mutating webhook | `"{{ gatekeeper_validatingwebhook_exempt_resources }}"` |
| `gatekeeper_mutatingwebhook_extra_resources` | Resources that are always added in the mutating webhook (even if exempted) | `[]` |
| `gatekeeper_controllermanager_memory_request` | | `128Mi` |
| `gatekeeper_controllermanager_cpu_request` | | `10m` |
| `gatekeeper_controllermanager_memory_limit` | | `512Mi` |
| `gatekeeper_controllermanager_cpu_limit` | | `null` |
| `gatekeeper_audit_memory_request` | | `128Mi` |
| `gatekeeper_audit_cpu_request` | | `200m` |
| `gatekeeper_audit_memory_limit` | | `1Gi` |
| `gatekeeper_audit_cpu_limit` | | `null` |
| `gatekeeper_chart_version` | | :page_with_curl: |
| `gatekeeper_image` | | :page_with_curl: |
| `gatekeeper_label_namespace_image` | | :page_with_curl: |
| `gatekeeper_probe_webhook_image` | | :page_with_curl: |
| `gatekeeper_library_version` | | :page_with_curl: |

Public variables for pod-security-policy mutation:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `gatekeeper_library_defaultallowprivilegeescalation_enabled` | | `true` |
| `gatekeeper_library_defaultallowprivilegeescalation_value` | | `false` |
| `gatekeeper_library_defaultcapabilities_enabled` | | `false` |
| `gatekeeper_library_defaultcapabilities_add` | | :page_with_curl: |
| `gatekeeper_library_defaultreadonlyrootfilesystem_enabled` | | `false` |
| `gatekeeper_library_defaultreadonlyrootfilesystem_value` | | `true` |
| `gatekeeper_library_defaultseccomp_enabled` | | `true` |
| `gatekeeper_library_defaultseccomp_value` | | `runtime/default` |
| `gatekeeper_library_defaultselinux_enabled` | | `false` |
| `gatekeeper_library_defaultselinux_value` | | :page_with_curl: |
| `gatekeeper_library_defaultfsgroup_enabled` | | `false` |
| `gatekeeper_library_defaultfsgroup_value` | | `3000` |
| `gatekeeper_library_defaultrunasnonroot_enabled` | | `true` |
| `gatekeeper_library_defaultrunasnonroot_value` | | `true` |
| `gatekeeper_library_defaultrunasgroup_enabled` | | `false` |
| `gatekeeper_library_defaultrunasgroup_value` | | `2000` |
| `gatekeeper_library_defaultrunasuser_enabled` | | `false` |
| `gatekeeper_library_defaultrunasuser_value` | | `1000` |
| `gatekeeper_library_defaultsupplementalgroups_enabled` | | `false` |
| `gatekeeper_library_defaultsupplementalgroups_value` | | `[3000]` |

Public variables for pod-security-policy admission:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `gatekeeper_library_allowprivilegeescalationcontainer_enforcementaction` | | `deny` |
| `gatekeeper_library_allowprivilegeescalationcontainer_exemptimages` | | `[]` |
| `gatekeeper_library_apparmor_enforcementaction` | | `deny` |
| `gatekeeper_library_apparmor_exemptimages` | | `[]` |
| `gatekeeper_library_apparmor_allowedprofiles` | | `[runtime/default]` |
| `gatekeeper_library_capabilities_enforcementaction` | | `deny` |
| `gatekeeper_library_capabilities_exemptimages` | | `[]` |
| `gatekeeper_library_capabilities_allowed_capabilities` | | `[]` |
| `gatekeeper_library_capabilities_required_drop_capabilities` | | `[]` |
| `gatekeeper_library_flexvolumes_enforcementaction` | | `deny` |
| `gatekeeper_library_forbiddensysctls_enforcementaction` | | `deny` |
| `gatekeeper_library_fsgroup_enforcementaction` | | `disabled` |
| `gatekeeper_library_hostfilesystem_enforcementaction` | | `deny` |
| `gatekeeper_library_hostnamespace_enforcementaction` | | `deny` |
| `gatekeeper_library_hostnetworkingports_enforcementaction` | | `deny` |
| `gatekeeper_library_hostnetworkingports_exemptimages` | | `[]` |
| `gatekeeper_library_privilegedcontainer_enforcementaction` | | `deny` |
| `gatekeeper_library_privilegedcontainer_exemptimages` | | `[]` |
| `gatekeeper_library_procmount_enforcementaction` | | `deny` |
| `gatekeeper_library_procmount_exemptimages` | | `[]` |
| `gatekeeper_library_readonlyrootfilesystem_enforcementaction` | | `disabled` |
| `gatekeeper_library_readonlyrootfilesystem_exemptimages` | | `[]` |
| `gatekeeper_library_seccomp_enforcementaction` | | `deny` |
| `gatekeeper_library_seccomp_exemptimages` | | `[]` |
| `gatekeeper_library_selinuxv2_enforcementaction` | | `deny` |
| `gatekeeper_library_selinuxv2_exemptimages` | | `[]` |
| `gatekeeper_library_allowedusers_enforcementaction` | | `deny` |
| `gatekeeper_library_allowedusers_exemptimages` | | `[]` |
| `gatekeeper_library_volumetypes_enforcementaction` | | `deny` |
| `gatekeeper_library_volumetypes_allow_csi` | | `false` |

Public variables for general admission:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `gatekeeper_library_allowedrepos_enforcementaction` | | `disabled` |
| `gatekeeper_library_allowedrepos_repos` | | :page_with_curl: |
| `gatekeeper_library_blockendpointeditdefaultrole_enforcementaction` | | `deny` |
| `gatekeeper_library_blockloadbalancer_enforcementaction` | | `deny` |
| `gatekeeper_library_blocknodeportservices_enforcementaction` | | `deny` |
| `gatekeeper_library_containerlimits_enforcementaction` | | `warn` |
| `gatekeeper_library_containerlimits_cpu` | | `500m` |
| `gatekeeper_library_containerlimits_memory` | | `4Gi` |
| `gatekeeper_library_containerratios_enforcementaction` | | `warn` |
| `gatekeeper_library_containerratios_ratio` | | `"4"` |
| `gatekeeper_library_containerratios_cpuratio` | | `"10"` |
| `gatekeeper_library_disallowedtags_enforcementaction` | | `warn` |
| `gatekeeper_library_disallowedtags_tags` | | `[latest, main, master]` |
| `gatekeeper_library_externalip_enforcementaction` | | `deny` |
| `gatekeeper_library_httpsonly_enforcementaction` | | `disabled` |
| `gatekeeper_library_imagedigests_enforcementaction` | | `deny` |
| `gatekeeper_library_imagedigests_namespaces` | | `[default]` |
| `gatekeeper_library_replicalimits_enforcementaction` | | `deny` |
| `gatekeeper_library_replicalimits_min_replicas` | | `1` |
| `gatekeeper_library_replicalimits_max_replicas` | | `50` |
| `gatekeeper_library_requiredprobes_enforcementaction` | | `dryrun` |
| `gatekeeper_library_uniqueingresshost_enforcementaction` | | `deny` |
| `gatekeeper_library_uniqueserviceselector_enforcementaction` | | `deny` |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `gatekeeper_projects` |  | :page_with_curl: |
