# DHCP

## Scope

The `dhcp` role installs an ISC-DHCP server.

:warning: The default range is from `.10` to `.40` in the subnet of the default interface. You can tune this with either:

- `dhcp_default_range_start` and `dhcp_default_range_size`, or
- `dhcp_default_range`, or
- `dhcp_subnets`

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/dhcp.yml).

Public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `dhcp_external_enabled` | Enable ISC-DCHP | When the `dhcp` group is not empty: `"{{ groups['dhcp'] \| default([]) \| length > 0 }}"` |
| `dhcp_interfaces_v4` | List of interfaces to listen to | `["{{ ansible_facts.default_ipv4.interface }}"]` |
| `dhcp_default_range_start` | Range start for the default pool | `10` |
| `dhcp_default_range_size` | Range size for the default pool | `30` |
| `dhcp_default_range` | Range for the default pool | :page_with_curl: |
| `dhcp_default_ntp_servers` | NTP servers for the default pool | `["{{ ansible_facts.default_ipv4.gateway }}"]` |
| `dhcp_subnets` | Subnets | :page_with_curl: |
