# Promtail

## Scope

The `promtail` role installs [Promtail](https://grafana.com/docs/loki/latest/clients/promtail/), as log
aggregation system.

## Extra pipeline stages

Example:

```yaml
promtail_extra_pipeline_stages:
- limit:
    rate: 100  # lines per second
    burst: 100
    drop: true
    by_label_name: namespace
```

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/promtail.yml).

GitOps variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `promtail_enabled` | Enable Promtail | `"{{ not opentelemetry_collector_enabled }}"` |
| `promtail_namespace_pod_security` | Pod Security of the instance namespace | `privileged` |
| `promtail_helm_dependencies` | Helm dependencies | :page_with_curl: |

Other public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `promtail_journald_enabled` | Enable journald support | `true` |
| `promtail_extra_pipeline_stages` | [Extra pipeline stages](#extra-pipeline-stages) | `[]` |
| `promtail_memory_request` | | `300Mi` |
| `promtail_cpu_request` | | `20m` |
| `promtail_memory_limit` | | `1Gi` |
| `promtail_cpu_limit` | | `null` |
| `promtail_chart_version` |  | :page_with_curl: |
| `promtail_image` |  | :page_with_curl: |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `promtail_projects` |  | :page_with_curl: |
