# Helm

## Scope

The `helm` role installs [Helm](https://helm.sh/).

This role is applied on the `gitops` and `kubespray` groups.

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/helm.yml).

Public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `helm_enabled` | Enable Helm installation | `true` |
| `helm_version` |  | :page_with_curl: |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `helm_projects` |  | :page_with_curl: |
| `helm_downloads` |  | :page_with_curl: |
