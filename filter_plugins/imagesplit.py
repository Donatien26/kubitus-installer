__metaclass__ = type

DOCUMENTATION = r'''
  name: imagesplit
  short_description: get parts from Docker image
  description:
    - Split an image name into its component parts.
  positional: _input, query
  options:
    _input:
      description: Image name.
      type: str
      required: true
    query:
      description: Specify output query.
      type: str
      choices:
        - all
        - repository,tag
        - registry,image,tag,digest
        - hostname
        - manifest_name
        - tag
        - digest
'''

EXAMPLES = r'''
    all: "{{ 'redis' | imagesplit }}"
    #  digest: null
    #  hostname: docker.io
    #  manifest_name: library/redis
    #  tag: null
    repository,tag: "{{ 'redis' | imagesplit('repository,tag') }}"
    #  repository: docker.io/library/redis
    #  tag: latest
    repository,tag2: "{{ 'quay.io/argoproj/argocd:v2.5.6@sha256:072f66b2cb79797178820385eb78dd419967c9905ec3ef840ddf6cfec7b1e3d4' | imagesplit('repository,tag') }}"
    #  repository: quay.io/argoproj/argocd
    #  tag: v2.5.6@sha256:072f66b2cb79797178820385eb78dd419967c9905ec3ef840ddf6cfec7b1e3d4
    registry,image,tag,digest: "{{ 'redis' | imagesplit('registry,image,tag,digest', default_digest=omit) }}"
    #  image: library/redis
    #  registry: docker.io
    #  tag: latest
'''

RETURN = r'''
  _value:
    description:
      - A dictionary with components as keyword and their value.
    type: any
'''

from collections import namedtuple
from re import search as re_search
from typing import Optional, Tuple

from ansible.errors import AnsibleFilterError
from ansible.utils import helpers

DOCKER_HOSTNAME = 'docker.io'

ReferenceParts = namedtuple('ReferenceParts', ['hostname', 'manifest_name', 'tag', 'digest'])


class FilterModule(object):
    '''Image parts filter.'''

    def filters(self):
        return {
            'imagesplit': self.imagesplit,
        }

    def imagesplit(
        self,
        value: str,
        query: str = 'all',
        default_tag: Optional[str] = 'latest',
        default_digest: Optional[str] = None,
        alias: str = 'imagesplit',
    ):
        parts = self.full_reference_parts(value)
        dict_parts = helpers.object_to_dict(parts, exclude=['count', 'index'])

        if parts.digest:
            sha = parts.digest[7:]
        else:
            sha = default_digest

        if query == 'all':
            return dict_parts
        elif query == 'registry,image,tag':
            tag = parts.tag or default_tag
            if parts.digest or default_digest:
                tag = '{0}@{1}'.format(tag, parts.digest or default_digest)
            return {
                'registry': parts.hostname,
                'image': parts.manifest_name,
                'tag': tag,
            }
        elif query == 'registry,image,tag,digest':
            return {
                'registry': parts.hostname,
                'image': parts.manifest_name,
                'tag': parts.tag or default_tag,
                'digest': parts.digest or default_digest,
            }
        elif query == 'registry,repository,tag':
            tag = parts.tag or default_tag
            if parts.digest or default_digest:
                tag = '{0}@{1}'.format(tag, parts.digest or default_digest)
            return {
                'registry': parts.hostname,
                'repository': parts.manifest_name,
                'tag': tag,
            }
        elif query == 'registry,repository,tag,digest':
            return {
                'registry': parts.hostname,
                'repository': parts.manifest_name,
                'tag': parts.tag or default_tag,
                'digest': parts.digest or default_digest,
            }
        elif query == 'registry,repository,tag,digest@sha':
            return {
                'registry': parts.hostname,
                'repository': parts.manifest_name,
                'tag': parts.tag or default_tag,
                'sha': parts.digest or default_digest,
            }
        elif query == 'registry,repository,tag,sha':
            return {
                'registry': parts.hostname,
                'repository': parts.manifest_name,
                'tag': parts.tag or default_tag,
                'sha': sha,
            }
        elif query == 'repository,tag':
            repository = '{0}/{1}'.format(parts.hostname, parts.manifest_name)
            tag = parts.tag or default_tag
            if parts.digest or default_digest:
                tag = '{0}@{1}'.format(tag, parts.digest or default_digest)
            return {
                'repository': repository,
                'tag': tag,
            }
        elif query == 'repository,tag,sha':
            return {
                'repository': '{0}/{1}'.format(parts.hostname, parts.manifest_name),
                'tag': parts.tag or default_tag,
                'sha': sha,
            }
        elif query in dict_parts:
            return dict_parts.get(query)
        raise AnsibleFilterError('{0}: unknown image part: {1}'.format(alias, query))

    @classmethod
    def full_reference_parts(cls, name: str) -> ReferenceParts:
        """Get full reference parts (hostname, manifest_name, tag, digest).

        Args:
            name: Reference name.

        Returns:
            The corresponding full reference parts.
        """
        hostname, remaining = cls._split_docker_domain(name)
        digest: Optional[str]
        tag: Optional[str]
        try:
            remaining, digest = remaining.split('@', 1)
        except ValueError:
            digest = None
        try:
            remaining, tag = remaining.split(':', 1)
        except ValueError:
            tag = None
        if hostname == DOCKER_HOSTNAME and '/' not in remaining:
            remaining = 'library/{0}'.format(remaining)
        return ReferenceParts(hostname, remaining, tag, digest)

    @classmethod
    def _split_docker_domain(cls, name: str) -> Tuple[str, str]:
        parts = name.split('/', 1)
        if len(parts) == 2 and re_search(r'^localhost$|:\d|\.', parts[0]):
            return parts[0], parts[1]
        return DOCKER_HOSTNAME, name
