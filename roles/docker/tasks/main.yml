- name: Docker
  tags:
  - docker
  - docker_main
  when:
  - 'true'
  block:
  - name: Docker | Gather min facts
    setup:
      gather_subset:
      - '!all'
      - min


# ###########################################################################
# Firewall
# ###########################################################################
  - name: Docker | Check if nftables is installed
    command:
      argv:
      - dpkg-query
      - --show
      - --showformat=${db:Status-Status}
      - nftables
    failed_when: false
    changed_when: false
    register: dpkg_query_nftables


  - name: Docker | Stop nftables
    service:
      name: nftables
      state: stopped
    become: true
    when: dpkg_query_nftables.stdout == 'installed'


  - name: Docker | Disable and mask nftables
    systemd:
      name: nftables
      enabled: false
      masked: true
    become: true
    when: dpkg_query_nftables.stdout == 'installed'


# ###########################################################################
# Docker
# ###########################################################################
  - name: Docker | Docker service configuration directory
    file:
      dest: /etc/systemd/system/docker.service.d
      state: directory
      owner: root
      group: root
      mode: '0755'
    become: true


  - name: Docker | Docker service proxy configuration
    template:
      src: docker-proxy.conf
      dest: /etc/systemd/system/docker.service.d/proxy.conf
      owner: root
      group: root
      mode: '0640'
    become: true


  - name: Docker | Docker repository key
    copy:
      src: roles/docker/files/docker.asc
      dest: /usr/share/keyrings/docker-archive-keyring.asc
      owner: root
      group: root
      mode: '0644'
    when: docker_channel != 'debian'
    become: true
    register: docker_repository_key


  - name: Docker | Docker repository proxy configuration
    copy:
      content: |
        Acquire::https::Proxy::download.docker.com "{{ (https_proxy | default('')) or 'DIRECT' }}";
      dest: /etc/apt/apt.conf.d/docker-proxy.conf
      owner: root
      group: root
      mode: '0644'
    when: docker_channel != 'debian'
    become: true
    register: docker_repository_proxy


  - name: Docker | Docker repository
    copy:
      content:
        deb
        [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.asc]
        https://download.docker.com/linux/debian
        {{ ansible_facts.distribution_release }}
        {{ docker_channel }}
      dest: /etc/apt/sources.list.d/docker-ce.list
      owner: root
      group: root
      mode: '0644'
    when: docker_channel != 'debian'
    become: true
    register: docker_repository_list


  - name: Docker | Cleanup files
    file:
      path: "{{ item }}"
      state: absent
    loop:
    - /usr/share/keyrings/docker-archive-keyring.asc
    - /etc/apt/apt.conf.d/docker-proxy.conf
    - /etc/apt/sources.list.d/docker-ce.list
    when: docker_channel == 'debian'
    become: true


  - name: Docker | Install AppArmor
    apt:
      name: apparmor
      state: present
    become: true


  - name: Docker | Install Docker package
    apt:
      name: "{{ docker_package }}"
      state: present
      update_cache: "{{ docker_repository_key.changed or docker_repository_proxy.changed or docker_repository_list.changed }}"
      install_recommends: false
    become: true
