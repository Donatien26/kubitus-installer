{{ $httpStatuses := ($.Files.Get "src/data.yaml" | fromYaml).httpStatuses -}}
apiVersion: monitoring.coreos.com/v1
kind: PrometheusRule
metadata:
  name: ingress-nginx
  labels:
    release: prometheus-stack
spec:
  groups:
  - name: IngressNginx
    rules:
    - alert: NginxLatencyHigh
      expr: histogram_quantile(0.99, sum(rate(nginx_http_request_duration_seconds_bucket[2m])) by (host, node, le)) > 3
      for: 2m
      labels:
        severity: warning
      annotations:
        summary: Nginx latency high (instance {{`{{`}} $labels.instance {{`}}`}})
        description: "Nginx p99 latency is higher than 3 seconds.\n  VALUE = {{`{{`}} $value {{`}}`}}\n  LABELS = {{`{{`}} $labels {{`}}`}}"
    {{- range $httpStatus := $httpStatuses }}
      {{- $httpStatusShort := $httpStatus.message | replace " " "" }}
      {{- $warningThreshold := $.Values | merge dict | dig (printf "status_%d_warning_threshold" $httpStatus.code) 5 }}
      {{- $criticalThreshold := $.Values | merge dict | dig (printf "status_%d_critical_threshold" $httpStatus.code) 10 }}
    - alert: Nginx{{ $httpStatusShort }}{{ $httpStatus.code }}
      expr: sum(rate(nginx_ingress_controller_requests{status="{{ $httpStatus.code }}"}[1m])) / sum(rate(nginx_ingress_controller_requests[1m])) * 100 > {{ $warningThreshold }}
      for: 1m
      labels:
        severity: warning
      annotations:
        summary: Nginx high HTTP {{ $httpStatus.code }} error rate (instance {{`{{`}} $labels.instance {{`}}`}})
        description: "Too many HTTP requests with status {{ $httpStatus.code }} [{{ $httpStatus.message }}] (> {{ $warningThreshold }}%).\n  VALUE = {{`{{`}} $value {{`}}`}}\n  LABELS = {{`{{`}} $labels {{`}}`}}"
    - alert: Nginx{{ $httpStatusShort }}{{ $httpStatus.code }}
      expr: sum(rate(nginx_ingress_controller_requests{status="{{ $httpStatus.code }}"}[1m])) / sum(rate(nginx_ingress_controller_requests[1m])) * 100 > {{ $criticalThreshold }}
      for: 1m
      labels:
        severity: critical
      annotations:
        summary: Nginx high HTTP {{ $httpStatus.code }} error rate (instance {{`{{`}} $labels.instance {{`}}`}})
        description: "Too many HTTP requests with status {{ $httpStatus.code }} [{{ $httpStatus.message }}] (> {{ $criticalThreshold }}%).\n  VALUE = {{`{{`}} $value {{`}}`}}\n  LABELS = {{`{{`}} $labels {{`}}`}}"
    {{- end }}
