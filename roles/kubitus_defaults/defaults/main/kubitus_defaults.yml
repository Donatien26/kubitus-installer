top_level_domain: k8s.example.org
default_ingress_class: nginx
kubitus_ingress_class: "{{ default_ingress_class }}"
default_ingress_certificate_issuer: "{% if cert_manager_enabled %}ca-issuer{% else %}none{% endif %}"
apiserver_fqdn: "apiserver.{{ top_level_domain }}"
apiserver_ingress_enabled: false
apiserver_ingress_class: "{{ kubitus_ingress_class }}"
apiserver_ingress_certificate_issuer: "{{ default_ingress_certificate_issuer }}"
apiserver_ingress_proxy_read_timeout: 2h

offline_enabled: "{{ gitlabracadabra_external_enabled }}"
sync_windows_binaries: false
github_token: ''
netpol_extra_fqdn2ips: {}
netpol_fqdn2ips: "{{ netpol_default_fqdn2ips | combine(netpol_extra_fqdn2ips) }}"
kube_pod_security_default_enforce: baseline

kubeconfig: "{{ kubespray_inventory_dir }}/kubespray-artifacts/admin.conf"
kubectl: "{% if kubectl_delegate_to in groups['kubespray'] | default([])
  %}{{ kubespray_inventory_dir }}/kubespray-artifacts/kubectl{%
  else
  %}kubectl{%
  endif
  %}"
kubectl_delegate_to: "{{ (groups['kubespray'] | default([]) |
  union(groups['gitops'] | default(['localhost'])))[0] }}"
kubectl_become: true
kubectl_user: kubitus
kubectl_group: kubitus

kube_oidc_auth: "{{ keycloak_enabled }}"
kube_oidc_url: "https://{{ keycloak_fqdn }}/auth/realms/default"
kube_oidc_client_id: kubernetes
kube_oidc_username_claim: preferred_username
kube_oidc_username_prefix: oidc-usr-
kube_oidc_groups_claim: groups
kube_oidc_groups_prefix: oidc-grp-

external_registries_default:
- cr.fluentbit.io
- docker.io
- gcr.io
- ghcr.io
- public.ecr.aws
- quay.io
- registry.access.redhat.com
- registry.developers.crunchydata.com
- registry.gitlab.com
- registry.k8s.io
external_registries_extra: []
external_registries: "{{ external_registries_default + external_registries_extra }}"
external_registry_mirror_patterns: |
  {%
    if offline_enabled
  %}{{
      ["https://" + gitlab_registry_fqdn + "/v2/external-registries/$external_registry"]
  }}{%
    else
  %}{{
    []
  }}{%
    endif
  %}
containerd_registries: |
  {%
    set containerd_registries = {}
  %}{%
    for external_registry in external_registries
  %}{%
      set external_registry_slug = external_registry | regex_replace('[^a-zA-Z0-9]', '-')
  %}{%
      set tmp = containerd_registries.update({
        external_registry: external_registry_mirror_patterns |
          map('regex_replace', '\$external_registry_slug', external_registry_slug) |
          map('regex_replace', '\$external_registry', external_registry)
      })
  %}{%
    endfor
  %}{{
    containerd_registries
  }}

kube_service_addresses: 10.233.0.0/18
kube_pods_subnet: 10.233.64.0/18
kube_network_node_prefix: 24

kube_controller_terminated_pod_gc_threshold: 100
kube_dns_stub_domains: {}
cni_bin_dir: "{% if terraform_gke_enabled
              %}/home/kubernetes/bin{%
              else
              %}/opt/cni/bin{%
              endif %}"
download_cache_dir: /var/opt/kubitus/downloads
kubitus_data_dir: /var/opt/kubitus/config/{{ kubitus_inventory_name }}
kubitus_credentials_dir: "{{ ansible_inventory_sources | last | dirname }}"

tenants: []
workload_clusters: []

kube_apiserver_memory_request:
kube_apiserver_memory_limit:
etcd_memory_request:
etcd_memory_limit:
kube_controller_manager_memory_request:
kube_controller_manager_memory_limit:
kube_scheduler_memory_request:
kube_scheduler_memory_limit:

ansible_python_interpreter: /usr/bin/python3
kubitus_inventory_name: "{{ ansible_inventory_sources | last | dirname | basename }}"
proxy_env:
  http_proxy: "{{ http_proxy | default('') }}"
  HTTP_PROXY: "{{ http_proxy | default('') }}"
  https_proxy: "{{ https_proxy | default('') }}"
  HTTPS_PROXY: "{{ https_proxy | default('') }}"
  no_proxy: "{{ no_proxy | default('') }}"
  NO_PROXY: "{{ no_proxy | default('') }}"
netpol_default_fqdn2ips: {}
