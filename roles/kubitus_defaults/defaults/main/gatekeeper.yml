# GitOps
gatekeeper_enabled: true
gatekeeper_namespace_pod_security: restricted
gatekeeper_cascading_deletion: null
gatekeeper_extra_namespaces:
- argocd
gatekeeper_helm_dependencies:
- name: gatekeeper
  version: "{{ gatekeeper_chart_version }}"
  repository: "{% if offline_enabled
    %}https://{{ gitlab_fqdn }}/api/v4/projects/external-packages%2Fgatekeeper/packages/helm/stable/{%
    else
    %}https://open-policy-agent.github.io/gatekeeper/charts{% endif %}"
gatekeeper_extra_cluster_resource_whitelist:
- group: templates.gatekeeper.sh
  kind: ConstraintTemplate
- group: constraints.gatekeeper.sh
  kind: '*'
- group: mutations.gatekeeper.sh
  kind: Assign
- group: mutations.gatekeeper.sh
  kind: AssignMetadata
gatekeeper_extra_namespace_resource_whitelist:
- group: argoproj.io
  kind: Application


gatekeeper_mutating_webhook_timeout_seconds: 1

gatekeeper_validatingwebhook_exempt_resources:
- ciliumendpoints
- ciliumidentities
- ciliumnodes
- customresourcedefinitions
- mutatingwebhookconfigurations
- nodes
- validatingwebhookconfigurations
gatekeeper_validatingwebhook_extra_resources:
- pods/ephemeralcontainers
- pods/exec
- pods/log
- pods/eviction
- pods/portforward
- pods/proxy
- pods/attach
- pods/binding
- deployments/scale
- replicasets/scale
- statefulsets/persistentvolumeclaim
- statefulsets/scale
- replicationcontrollers/scale
- services/proxy
- nodes/proxy
- services/status
gatekeeper_mutatingwebhook_exempt_resources: "{{ gatekeeper_validatingwebhook_exempt_resources }}"
gatekeeper_mutatingwebhook_extra_resources: []

# controllerManager resources:
gatekeeper_controllermanager_memory_request: 128Mi
gatekeeper_controllermanager_cpu_request: 10m
gatekeeper_controllermanager_memory_limit: 512Mi
gatekeeper_controllermanager_cpu_limit:

# audit resources:
gatekeeper_audit_memory_request: 128Mi
gatekeeper_audit_cpu_request: 200m
gatekeeper_audit_memory_limit: 1Gi
gatekeeper_audit_cpu_limit:

gatekeeper_chart_version: 3.15.1
gatekeeper_image: openpolicyagent/gatekeeper:v3.15.1@sha256:aa61dcbebccef46e19e88e66dcb401c58debcb00b2bcbfc796dac0abfac91d37
gatekeeper_label_namespace_image: openpolicyagent/gatekeeper-crds:v3.15.1@sha256:4a972daee5a2efeabf68334983b157dc3021626c5b2f56b159bd88ec3b43716e
gatekeeper_probe_webhook_image: curlimages/curl:8.7.1@sha256:25d29daeb9b14b89e2fa8cc17c70e4b188bca1466086907c2d9a4b56b59d8e21
gatekeeper_library_version: 377cb915dba2db10702c25ef1ee374b4aa8d347a


# library/mutation/pod-security-policy tuning:
gatekeeper_library_defaultallowprivilegeescalation_enabled: true
gatekeeper_library_defaultallowprivilegeescalation_value: false
gatekeeper_library_defaultcapabilities_enabled: false
gatekeeper_library_defaultcapabilities_add:
- NEW_CAPABILITY
gatekeeper_library_defaultreadonlyrootfilesystem_enabled: false
gatekeeper_library_defaultreadonlyrootfilesystem_value: true
gatekeeper_library_defaultseccomp_enabled: true
gatekeeper_library_defaultseccomp_value: runtime/default
gatekeeper_library_defaultselinux_enabled: false
gatekeeper_library_defaultselinux_value:
  level: s1:c234,c567
  user: sysadm_u
  role: sysadm_r
  type: svirt_lxc_net_t
gatekeeper_library_defaultfsgroup_enabled: false
gatekeeper_library_defaultfsgroup_value: 3000
gatekeeper_library_defaultrunasnonroot_enabled: true
gatekeeper_library_defaultrunasnonroot_value: true
gatekeeper_library_defaultrunasgroup_enabled: false
gatekeeper_library_defaultrunasgroup_value: 2000
gatekeeper_library_defaultrunasuser_enabled: false
gatekeeper_library_defaultrunasuser_value: 1000
gatekeeper_library_defaultsupplementalgroups_enabled: false
gatekeeper_library_defaultsupplementalgroups_value:
- 3000

# library/pod-security-policy tuning:
gatekeeper_library_allowprivilegeescalationcontainer_enforcementaction: deny
gatekeeper_library_allowprivilegeescalationcontainer_exemptimages: []
gatekeeper_library_apparmor_enforcementaction: deny
gatekeeper_library_apparmor_exemptimages: []
gatekeeper_library_apparmor_allowedprofiles:
- runtime/default
gatekeeper_library_capabilities_enforcementaction: deny
gatekeeper_library_capabilities_exemptimages: []
gatekeeper_library_capabilities_allowed_capabilities: []
gatekeeper_library_capabilities_required_drop_capabilities: []
gatekeeper_library_flexvolumes_enforcementaction: deny
gatekeeper_library_forbiddensysctls_enforcementaction: deny
gatekeeper_library_fsgroup_enforcementaction: disabled
gatekeeper_library_hostfilesystem_enforcementaction: deny
gatekeeper_library_hostnamespace_enforcementaction: deny
gatekeeper_library_hostnetworkingports_enforcementaction: deny
gatekeeper_library_hostnetworkingports_exemptimages: []
gatekeeper_library_privilegedcontainer_enforcementaction: deny
gatekeeper_library_privilegedcontainer_exemptimages: []
gatekeeper_library_procmount_enforcementaction: deny
gatekeeper_library_procmount_exemptimages: []
gatekeeper_library_readonlyrootfilesystem_enforcementaction: disabled
gatekeeper_library_readonlyrootfilesystem_exemptimages: []
gatekeeper_library_seccomp_enforcementaction: deny
gatekeeper_library_seccomp_exemptimages: []
gatekeeper_library_selinuxv2_enforcementaction: deny
gatekeeper_library_selinuxv2_exemptimages: []
gatekeeper_library_allowedusers_enforcementaction: deny
gatekeeper_library_allowedusers_exemptimages: []
gatekeeper_library_volumetypes_enforcementaction: deny
gatekeeper_library_volumetypes_allow_csi: false

# library/general tuning:
gatekeeper_library_allowedrepos_enforcementaction: disabled
gatekeeper_library_allowedrepos_repos:
- docker.io/
- ghcr.io/
- quay.io/
- registry.gitlab.com/kubitus-project/
- registry.k8s.io/
gatekeeper_library_blockendpointeditdefaultrole_enforcementaction: deny
gatekeeper_library_blockloadbalancer_enforcementaction: deny
gatekeeper_library_blocknodeportservices_enforcementaction: deny
gatekeeper_library_containerlimits_enforcementaction: warn
gatekeeper_library_containerlimits_cpu: 500m
gatekeeper_library_containerlimits_memory: 4Gi
gatekeeper_library_containerratios_enforcementaction: warn
gatekeeper_library_containerratios_ratio: "4"
gatekeeper_library_containerratios_cpuratio: "10"
gatekeeper_library_disallowedtags_enforcementaction: warn
gatekeeper_library_disallowedtags_tags:
- latest
- main
- master
gatekeeper_library_externalip_enforcementaction: deny
gatekeeper_library_httpsonly_enforcementaction: disabled
gatekeeper_library_imagedigests_enforcementaction: deny
gatekeeper_library_imagedigests_namespaces:
- default
gatekeeper_library_replicalimits_enforcementaction: deny
gatekeeper_library_replicalimits_min_replicas: 0
gatekeeper_library_replicalimits_max_replicas: 50
gatekeeper_library_requiredprobes_enforcementaction: dryrun
gatekeeper_library_uniqueingresshost_enforcementaction: deny
gatekeeper_library_uniqueserviceselector_enforcementaction: deny


gatekeeper_projects:
  external-packages/gatekeeper:
    extends: .packages
    package_mirrors:
    - helm:
        repo_url: https://open-policy-agent.github.io/gatekeeper/charts
        package_name: gatekeeper
        versions:
        - "{{ gatekeeper_chart_version }}"

  external-registries/docker.io:
    extends: .registry
    image_mirrors:
    - from: "{{ gatekeeper_image }}"
    - from: "{{ gatekeeper_label_namespace_image }}"
    - from: "{{ gatekeeper_probe_webhook_image }}"

  external-repositories/github.com/open-policy-agent/gatekeeper-library:
    extends: .mirror
    mirrors:
    - url: https://github.com/open-policy-agent/gatekeeper-library.git
      branches:
      - from: master
