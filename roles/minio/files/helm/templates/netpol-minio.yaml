apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: default-minio
spec:
  podSelector: {}
  policyTypes:
  - Ingress
  - Egress
  egress:
  - to:
      - namespaceSelector:
          matchLabels:
            kubernetes.io/metadata.name: kube-system
        podSelector:
          matchLabels:
            k8s-app: kube-dns
      - ipBlock:
          cidr: 169.254.25.10/32
    ports:
    - port: 53
      protocol: TCP
    - port: 53
      protocol: UDP
---
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: minio-job
  annotations:
    helm.sh/hook: pre-install,pre-upgrade
    helm.sh/hook-delete-policy: before-hook-creation,hook-succeeded
spec:
  podSelector:
    matchLabels:
      app: minio-job
  policyTypes:
    - Ingress
    - Egress
  egress:
    # to minio:9000
    - to:
        - podSelector:
            matchLabels:
                app: minio
      ports:
        - port: 9000
          protocol: TCP
---
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: minio
spec:
  podSelector:
    matchLabels:
      app: minio
  policyTypes:
    - Ingress
    - Egress
  ingress:
    - from:
        # from minio to :9000
        - podSelector:
            matchLabels:
                app: minio
        # from minio-job to :9000
        - podSelector:
            matchLabels:
                app: minio-job
        # from prometheus to :9000
        - namespaceSelector:
            matchLabels:
              kubernetes.io/metadata.name: prometheus-stack
          podSelector:
            matchLabels:
              app.kubernetes.io/name: prometheus
        {{ with .Values.minio.ingress.ingressClassName }}
        # from ingress-nginx to :9000
        - namespaceSelector:
            matchLabels:
              kubernetes.io/metadata.name: ingress-{{ . }}
          podSelector:
            matchLabels:
              app.kubernetes.io/part-of: ingress-nginx
              app.kubernetes.io/name: ingress-nginx
              app.kubernetes.io/instance: ingress-{{ . }}
              app.kubernetes.io/component: controller
        {{ end }}
      ports:
        - port: 9000
          protocol: TCP
    {{ with .Values.minio.consoleIngress.ingressClassName }}
    # from ingress-nginx to :9001
    - from:
        - namespaceSelector:
            matchLabels:
              kubernetes.io/metadata.name: ingress-{{ . }}
          podSelector:
            matchLabels:
              app.kubernetes.io/part-of: ingress-nginx
              app.kubernetes.io/name: ingress-nginx
              app.kubernetes.io/instance: ingress-{{ . }}
              app.kubernetes.io/component: controller
      ports:
        - port: 9001
          protocol: TCP
    {{ end }}
  egress:
    # to minio:9000
    - to:
        - podSelector:
            matchLabels:
                app: minio
      ports:
        - port: 9000
          protocol: TCP
    # to self:443
    {{- with .Values.self_ips }}
    - to:
        {{- range $ip := . }}
        - ipBlock:
            cidr: {{ $ip }}/32
        {{- end }}
      ports:
        - port: 443
          protocol: TCP
    {{- end }}
    # to keycloack:443
    {{- with .Values.oidc_ips }}
    - to:
        {{- range $ip := . }}
        - ipBlock:
            cidr: {{ $ip }}/32
        {{- end }}
      ports:
        - port: 443
          protocol: TCP
    {{- end }}
---
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: minio-post-job
  annotations:
    helm.sh/hook: post-install,post-upgrade
    helm.sh/hook-delete-policy: before-hook-creation,hook-succeeded
spec:
  podSelector:
    matchLabels:
      app: minio-post-job
  policyTypes:
    - Ingress
    - Egress
  egress:
    # to minio:9000
    - to:
        - podSelector:
            matchLabels:
                app: minio
      ports:
        - port: 9000
          protocol: TCP
