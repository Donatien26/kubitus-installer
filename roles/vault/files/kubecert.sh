#!/bin/bash

# set -x
set -e

# Configuration with env
# VAULT_ADDR=
# VAULT_SKIP_VERIFY=
KUBECERT_ISSUE_ENDPOINT="${KUBECERT_ISSUE_ENDPOINT:-pki-k8s/issue/k8s}"
KUBECERT_COMMON_NAME_COMMAND="${KUBECERT_COMMON_NAME_COMMAND:-echo "\$USERNAME"}"

# Variables
common_name="$(eval "$KUBECERT_COMMON_NAME_COMMAND")"
vault_slug="$(echo "$common_name@$VAULT_ADDR"  | base64  | tr -d '\n')"
cache_dir="$HOME/.kube/kubecert"
mkdir -p "$cache_dir"

# Print cached certificate if valid
valid_cached_certificate() {
    local certificate
    certificate="$(cat "$cache_dir/$vault_slug" 2>/dev/null)"
    [ -n "$certificate" ] || return 1
    certificate_expiration="$(echo "$certificate" | grep '"expiration":' | awk -F' ' '{print $2}' | sed 's/,$//')"
    [ "$certificate_expiration" -gt "$(date +%s)" ] || return 1
    echo "$certificate"
}

# Issue and print certificate
new_certificate() {
    export VAULT_TOKEN="$(vault login -method=oidc $vault_tls_skip_verify -no-store -field=token)"

    vault write $vault_tls_skip_verify -format=json "$KUBECERT_ISSUE_ENDPOINT" common_name="$common_name" | tee "$cache_dir/$vault_slug"
}

# Get certificate from cache or issue a new one
get_certificate() {
    valid_cached_certificate || new_certificate
}

print_exec_credential() {
    local certificate certificate_expiration expiration_timestamp
    certificate="$(get_certificate)"
    certificate_expiration="$(echo "$certificate" | grep '"expiration":' | awk -F' ' '{print $2}' | sed 's/,$//')"
    expiration_timestamp="$(TZ=UTC date -d @$certificate_expiration  +%Y-%m-%dT%H:%M:%SZ)"

    echo -n '{'
        echo -n '"kind":"ExecCredential",'
        echo -n '"apiVersion":"client.authentication.k8s.io/v1",'
        echo -n '"spec":{'
            echo -n '"interactive":false'
        echo -n '},'
        echo -n '"status":{'
            echo -n '"expirationTimestamp":"'$expiration_timestamp'",'
            echo -n '"clientCertificateData":"'
                echo "$certificate" | grep '"certificate":' | awk -F '"' '{print $4}' | tr -d '\n'
            echo -n '",'
            echo -n '"clientKeyData":"'
                echo "$certificate" | grep '"private_key":' | awk -F '"' '{print $4}' | tr -d '\n'
            echo -n '"'
        echo -n '}'
    echo -n '}'
}

print_exec_credential
