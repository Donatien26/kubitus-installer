gitops_instance_variables:
  fqdn: {{ gitops_instance_variables.fqdn | to_json }}
  cert_manager: {{ gitops_instance_variables.cert_manager | to_json }}
  {% if gitops_instance_variables.transit_address %}
  transit_tls_ca_crt: {{ gitops_instance_variables.transit_tls_ca_crt | to_json }}
  {% endif %}

# https://learn.hashicorp.com/tutorials/vault/kubernetes-raft-deployment-guide#configure-vault-helm-chart
# https://github.com/hashicorp/vault-helm/blob/main/values.yaml
vault:
  global:
    # TLS for end-to-end encrypted transport
    tlsDisable: false

    {% if gitops_instance_variables.telemetry_enabled and prometheus_stack_enabled %}
    serverTelemetry:
      # Enable integration with the Prometheus Operator
      # See the top level serverTelemetry section before enabling this feature.
      prometheusOperator: true
    {% endif %}


  injector:
    # True if you want to enable vault agent injection.
    # @default: global.enabled
    enabled: {{ gitops_instance_variables.injector_enabled | to_json }}

    # image sets the repo and tag of the vault-k8s image to use for the injector.
    image:
      {{ gitops_instance_variables.k8s_image | imagesplit('repository,tag') | to_nice_yaml(indent=2) | indent(width=6) }}

    # agentImage sets the repo and tag of the Vault image to use for the Vault Agent
    # containers. This should de set to the official Vault image. Vault 1.3.1+ is
    # required.
    agentImage:
      {{ gitops_instance_variables.image | imagesplit('repository,tag') | to_nice_yaml(indent=2) | indent(width=6) }}

    resources:
      requests:
        memory: 256Mi
        cpu: 250m
      limits:
        memory: 256Mi
        cpu: 250m

    # Configure the log format of the injector. Supported log formats: "standard", "json".
    logFormat: "json"


  server:
    # extraArgs: "-log-level=debug"
    image:
      {{ gitops_instance_variables.image | imagesplit('repository,tag') | to_nice_yaml(indent=2) | indent(width=6) }}

    {% if gitops_instance_variables.transit_address %}
    # Configure the Update Strategy Type for the StatefulSet
    # See https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/#update-strategies
    updateStrategyType: RollingUpdate

    {% endif %}
    # Configure the logging format for the Vault Server.
    # Supported log formats include: standard, json
    logFormat: "json"

    resources:
      requests:
        memory: {{ gitops_instance_variables.server_memory_request | to_json }}
        cpu: {{ gitops_instance_variables.server_cpu_request | to_json }}
      limits:
        memory: {{ gitops_instance_variables.server_memory_limit | to_json }}
        cpu: {{ gitops_instance_variables.server_cpu_limit | to_json }}

    # Ingress allows ingress services to be created to allow external access
    # from Kubernetes to access Vault pods.
    # If deployment in on OpenShift, the following block is ignored.
    # In order to expose the service, use the route section below
    ingress:
      enabled: {{ gitops_instance_variables.ingress_enabled | to_json }}
      annotations:
        nginx.ingress.kubernetes.io/backend-protocol: HTTPS
        {% if gitops_instance_variables.ingress_certificate_issuer != 'none' %}
        cert-manager.io/cluster-issuer: {{ gitops_instance_variables.ingress_certificate_issuer | to_json }}
        {% endif %}
      # Optionally use ingressClassName instead of deprecated annotation.
      # See: https://kubernetes.io/docs/concepts/service-networking/ingress/#deprecated-annotation
      ingressClassName: {{ gitops_instance_variables.ingress_class | to_json }}
      hosts:
        - host: {{ gitops_instance_variables.fqdn | to_json }}
      tls:
        - hosts:
          - {{ gitops_instance_variables.fqdn | to_json }}
          {% if gitops_instance_variables.ingress_certificate_issuer != 'none' %}
          secretName: "{{ gitops_instance_variables.fqdn }}-tls"
          {% endif %}

    # For HA configuration and because we need to manually init the vault,
    # we need to define custom readiness/liveness Probe settings
    # Used to define custom readinessProbe settings
    readinessProbe:
      enabled: true
      path: "/v1/sys/health?standbyok=true&sealedcode=204&uninitcode=204"
    # Used to enable a livenessProbe for the pods
    livenessProbe:
      enabled: true
      path: "/v1/sys/health?standbyok=true"
      # initialDelaySeconds: 60

    # extraEnvironmentVars is a list of extra environment variables to set with the stateful set. These could be
    # used to include variables required for auto-unseal.
    extraEnvironmentVars:
      VAULT_CACERT: /vault/userconfig/tls-server/ca.crt

    # Note: Configuration files are stored in ConfigMaps so sensitive data
    # such as passwords should be either mounted through extraSecretEnvironmentVars
    # or through a Kube secret.
    extraSecretEnvironmentVars:
      - envName: VAULT_TOKEN
        secretName: kubitus
        secretKey: VAULT_TOKEN

    # Deprecated: please use 'volumes' instead.
    # extraVolumes is a list of extra volumes to mount. These will be exposed
    # to Vault in the path `/vault/userconfig/<name>/`. The value below is
    # an array of objects.
    extraVolumes:
    - type: secret
      name: tls-server
    # - type: secret
    #   name: kms-creds
    {% if gitops_instance_variables.transit_address and gitops_instance_variables.transit_tls_ca_crt %}
    - type: configMap
      name: kubitus-transit-tls
    {% endif %}


    # This configures the Vault Statefulset to create a PVC for audit
    # logs. Once Vault is deployed, initialized, and unsealed, Vaul must
    # be configured to use this for audit logs. Thiss will be mounted to
    # /vault/audit
    # See https://www.vaultproject.io/docs/audit/index.html to know more
    auditStorage:
      enabled: true


    standalone:
      enabled: false


    # Run Vault in "HA" mode.
    ha:
      enabled: true
      replicas: {{ gitops_instance_variables.ha_replicas | to_json }}
      raft:
        enabled: true
        setNodeId: true
        config: |
          ui = true
          listener "tcp" {
            address = "[::]:8200"
            cluster_address = "[::]:8201"
            tls_cert_file = "/vault/userconfig/tls-server/tls.crt"
            tls_key_file = "/vault/userconfig/tls-server/tls.key"
            {% if gitops_instance_variables.telemetry_enabled %}
            telemetry {
              unauthenticated_metrics_access = true
            }
            {% endif %}
          }

          storage "raft" {
            path = "/vault/data"
            retry_join {
              leader_api_addr = "https://vault-0.vault-internal:8200"
              leader_ca_cert_file = "/vault/userconfig/tls-server/ca.crt"
              leader_client_cert_file = "/vault/userconfig/tls-server/tls.crt"
              leader_client_key_file = "/vault/userconfig/tls-server/tls.key"
            }
            retry_join {
              leader_api_addr = "https://vault-1.vault-internal:8200"
              leader_ca_cert_file = "/vault/userconfig/tls-server/ca.crt"
              leader_client_cert_file = "/vault/userconfig/tls-server/tls.crt"
              leader_client_key_file = "/vault/userconfig/tls-server/tls.key"
            }
            retry_join {
              leader_api_addr = "https://vault-2.vault-internal:8200"
              leader_ca_cert_file = "/vault/userconfig/tls-server/ca.crt"
              leader_client_cert_file = "/vault/userconfig/tls-server/tls.crt"
              leader_client_key_file = "/vault/userconfig/tls-server/tls.key"
            }
            retry_join {
              leader_api_addr = "https://vault-3.vault-internal:8200"
              leader_ca_cert_file = "/vault/userconfig/tls-server/ca.crt"
              leader_client_cert_file = "/vault/userconfig/tls-server/tls.crt"
              leader_client_key_file = "/vault/userconfig/tls-server/tls.key"
            }
            retry_join {
              leader_api_addr = "https://vault-4.vault-internal:8200"
              leader_ca_cert_file = "/vault/userconfig/tls-server/ca.crt"
              leader_client_cert_file = "/vault/userconfig/tls-server/tls.crt"
              leader_client_key_file = "/vault/userconfig/tls-server/tls.key"
            }

            autopilot {
              cleanup_dead_servers = "true"
              last_contact_threshold = "200ms"
              last_contact_failure_threshold = "10m"
              max_trailing_logs = 250000
              min_quorum = {{ gitops_instance_variables.ha_replicas | to_json }}
              server_stabilization_time = "10s"
            }

          }

          {% if gitops_instance_variables.transit_address %}
          seal "transit" {
            address = {{ gitops_instance_variables.transit_address | to_json }}
            key_name = {{ gitops_instance_variables.transit_key_name | to_json }}
            mount_path = {{ gitops_instance_variables.transit_mount_path | to_json }}
            {% if gitops_instance_variables.transit_tls_ca_crt %}
            tls_ca_cert = "/vault/userconfig/kubitus-transit-tls/ca.crt"
            tls_server_name = {{ gitops_instance_variables.transit_address | urlsplit('hostname') | to_json }}
            {% else %}
            tls_skip_verify = "true"
            {% endif %}
          }

          {% endif %}
          {% if gitops_instance_variables.telemetry_enabled %}
          telemetry {
            prometheus_retention_time = "30s"
            disable_hostname = true
            prefix_filter = ["+vault.token"]
          }

          {% endif %}
          service_registration "kubernetes" {}


  ui:
    enabled: {{ gitops_instance_variables.ui_enabled | to_json }}
    serviceType: {{ gitops_instance_variables.ui_service_type | to_json }}
    {% if gitops_instance_variables.ui_load_balancer_ip %}
    annotations:
      metallb.universe.tf/loadBalancerIPs: {{ gitops_instance_variables.ui_load_balancer_ip | to_json }}
    {% endif %}


  csi:
    enabled: {{ gitops_instance_variables.csi_enabled | to_json }}

    image:
      {{ gitops_instance_variables.csi_provider_image | imagesplit('repository,tag') | to_nice_yaml(indent=2) | indent(width=6) }}

    agent:
      image:
        {{ gitops_instance_variables.image | imagesplit('repository,tag') | to_nice_yaml(indent=2) | indent(width=8) }}

  {% if gitops_instance_variables.telemetry_enabled and prometheus_stack_enabled %}
  serverTelemetry:
    serviceMonitor:
      enabled: true
      selectors:
        release: prometheus-stack

    prometheusRules:
      enabled: true
      selectors:
        release: prometheus-stack
      rules:
        {% raw %}
        - alert: VaultHighResponseTime
          annotations:
            message: The response time of Vault is over 500ms on average over the last 5 minutes.
          expr: vault_core_handle_request{quantile="0.5"} > 500
          for: 5m
          labels:
            severity: warning
        - alert: VaultHighResponseTime
          annotations:
            message: The response time of Vault is over 1s on average over the last 5 minutes.
          expr: vault_core_handle_request{quantile="0.5"} > 1000
          for: 5m
          labels:
            severity: critical
        - alert: VaultSealed
          expr: vault_core_unsealed == 0
          for: 0m
          labels:
            severity: critical
          annotations:
            summary: Vault sealed (instance {{ $labels.instance }})
            description: "Vault instance is sealed on {{ $labels.instance }}\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}"
#        - alert: VaultTooManyPendingTokens
#          expr: avg(vault_token_create_count - vault_token_store_count) > 0
#          for: 5m
#          labels:
#            severity: warning
#          annotations:
#            summary: Vault too many pending tokens (instance {{ $labels.instance }})
#            description: "Too many pending tokens {{ $labels.instance }}: {{ $value | printf \"%.2f\"}}%\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}"
        - alert: VaultTooManyInfinityTokens
          expr: vault_token_count_by_ttl{creation_ttl="+Inf"} > 3
          for: 5m
          labels:
            severity: warning
          annotations:
            summary: Vault too many infinity tokens (instance {{ $labels.instance }})
            description: "Too many infinity tokens {{ $labels.instance }}: {{ $value | printf \"%.2f\"}}%\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}"
        - alert: VaultClusterHealth
          expr: sum(vault_core_active) / count(vault_core_active) <= 0.5
          for: 0m
          labels:
            severity: critical
          annotations:
            summary: Vault cluster health (instance {{ $labels.instance }})
            description: "Vault cluster is not healthy {{ $labels.instance }}: {{ $value | printf \"%.2f\"}}%\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}"
        {% endraw %}
  {% endif %}
