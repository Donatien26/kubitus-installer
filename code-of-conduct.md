# Kubitus Project Code of Conduct

All Kubitus projects follow the [CNCF Code of Conduct](https://github.com/cncf/foundation/blob/main/code-of-conduct.md).

Instances of abusive, harassing, or otherwise unacceptable behavior may be reported by creating
[a confidential issue](https://gitlab.com/kubitus-project/kubitus-installer/-/issues/new?issue[confidential]=true).
